/**
 * Created by M. on 01.04.2016.
 * Last edit: 06.06.2016 22:10
 */

var app =  angular.module( "kbExplorer" );

app.factory( 'authInterceptor', function ( $rootScope, $q, $location, $sessionStorage ) {
	return {
		// Add authorization token to headers
		request: function ( config ) {
			config.headers = config.headers || {};
			var token = $sessionStorage.tokenData;
			if( token ) {
				config.headers["Content-Type"] = 'application/json;UTF-8';
				config.headers.Authorization = 'Bearer ' + token.access_token;
			}
			return config;
		}
	};
});

app.factory( 'authExpiredInterceptor', [
	'$rootScope',
	'$q',
	'$injector',
	'$sessionStorage',
	'Tokens',
	function ( $rootScope, $q, $injector, $sessionStorage, Tokens ) {
	return {
		responseError: function ( response ) {
			// token has expired

			if( response.status == 403 ){
				console.log('ni masz dost�pu');
			}
			if ( response.data.path != "/oauth/token" && response.status === 401 && ( response.data.error == 'invalid_token' || response.data.error == 'Unauthorized' ) ) {

				//tu refresz tokena
				console.log('Token expired');
				/*Tokens.refresh().$promise.then(
					function success(response){
						console.log(response.status);
						$sessionStorage.tokenData = response.data;
					},
					function error(response){
						console.log(response);
					}
				);*/

			}
			return $q.reject( response );
		}
	};
}]);


app.config( function( $httpProvider ) {
	console.log( 'auth inceptor set up..' );
	$httpProvider.interceptors.push( 'authInterceptor' );
});