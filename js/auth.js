/**
 * Created by M. on 24.03.2016.
 * * Last edit: 15.04.2016 20:55
 */
var auth = angular.module('auth',[
        'ngStorage',
    ])
    .config([/*'$routeProvider',*/ '$locationProvider',
        function (/*$routeProvider,*/ $locationProvider) {

            $locationProvider.html5Mode(true);

        }
    ])
    .run();
auth.controller('authCtrl',[
    '$scope',
    '$location',
    '$http',
    '$sessionStorage',
    '$window',
    function($scope, $location, $http, $sessionStorage, $window) {

        $http.defaults.headers.post = {'Accept': 'application/json'};
        var authCode = $location.search();

        $http.post(_APP.auth.requestUrl + authCode.code + _APP.auth.redirectUrl, "", {
            headers: {'Authorization': 'Basic ' + _APP.auth.authHeader}
        })
        .then(function successCallback(response) {
            console.log(response);
            $sessionStorage.tokenData = response.data;

            $window.location.href = '/~msiminski/#/?success';

        }, function errorCallback(response) {
            console.log(response);
            $window.location.href = '/~msiminski/#/?error';
        });


    }]
);