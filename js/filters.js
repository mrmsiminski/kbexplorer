/**
 * Created by M. on 07.06.2016.
 * Last edit: 07.06.2016 18:00
 */

angular
    .module( 'kbExplorer' ).filter('attributeNameFilter', function() {
        return function( data ) {
            for ( var i = 0; i < $rootScope.attributesTable.length; i++ ){

                if( $rootScope.attributesTable[i].name == data ) return false;
            }
            return true;
        };
    });

angular
    .module( 'kbExplorer' ).filter('valueNameFilter', function() {
        return function( data, name ) {
            for ( var i = 0; i < data; i++ ){

                if( data[i].name == name ) return false;
            }
            return true;
        };
    });