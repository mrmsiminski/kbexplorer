
/**
 * Created by M. on 12.11.2015.
 * * Last edit: 17.07.2016 00:19
 */
(function(){

    var app = angular.module("kbExplorer",[
                'ui.router',
                'ngStorage',
                'ngMessages',
                'ngResource',
                'xeditable',
                'ui.select'
            ])
            .config([
                '$urlRouterProvider',
                '$stateProvider',
                '$locationProvider',
                function ($urlRouterProvider, $stateProvider, $locationProvider, $state) {

                    /*$locationProvider.html5Mode({
                        enabled: true,
                        requireBase: true,
                        rewriteLinks: false
                    });*/
                    $urlRouterProvider.otherwise('/');
                    $stateProvider
                       .state('attributes',{
                           url:'/attributes',
                           templateUrl: 'assets/views/attributes.html',
                           controller: 'attributesCtrl as attributes'
                       })
                       .state('base', {
                           url: '/base',
                           templateUrl: 'assets/views/base.html',
                           controller: 'baseCtrl as base'//,
                            /*resolve: {
                                fromState: function () {
                                    return {name: $state.fromState()};
                                }
                            }*/
                       })
                       .state('error',{
                           url:'/error',
                           templateUrl: 'assets/views/error.html',
                           //controller: ''
                       })
                       .state('facts',{
                           url:'/facts',
                           templateUrl: 'assets/views/facts.html',
                           controller: 'factsCtrl as facts'
                       })
                       .state('home', {
                            url: '/home',
                            templateUrl: 'assets/views/home.html',
                            controller: 'homeCtrl as home',
                            onEnter: function(){
                                //console.log('Halo? Can you hear me?');
                            }
                       })
                       .state('inference', {
                            url: '/inference',
                            templateUrl: 'assets/views/inference.html',
                            controller: 'inferenceCtrl as inference',
                            onEnter: function(){
                                //console.log('Halo? Can you hear me?');
                            }
                       })
                       .state('login', {
                            url: '/',
                            templateUrl: 'assets/views/login.html',
                            controller: 'loginCtrl as login'
                        })
                       .state('registration',{
                            url:'/registration',
                            templateUrl: 'assets/views/registration.html',
                            controller: 'registrationCtrl as registration'
                       })
                       .state('rules',{
                           url:'/rules',
                           templateUrl: 'assets/views/rules.html',
                           controller: 'rulesCtrl as rules'
                       })
                       .state('verification',{
                            url:'/verification',
                            templateUrl: 'assets/views/verification.html',
                            controller: 'verificationCtrl as verify'//,
                            //controllerAs: 'verify'
                        })
                       .state('visualisation',{
                            url:'/visualisation',
                            templateUrl: 'assets/views/bananaCode.html',
                            controller: 'visualisationCtrl as visual'
                        })
                       .state('auth',{
                           url:'/auth',
                           templateUrl: 'assets/views/auth2.html',
                           //controller: 'authCtrl'
                        }
                    );
                }
            ])
            .run([
                '$state',
                '$sessionStorage',
                '$rootScope',
                '$http',
                'Tokens',
                '$timeout',
                function($state, $sessionStorage, $rootScope, $http, Tokens, $timeout){

                    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

                        $("div.modal-backdrop").remove();
                        $("body").removeClass('modal-open');

                        //ATTRIBUTES
                        if( toState.name == 'attributes'){
                            if( typeof $sessionStorage.tokenData == "undefined" ){
                                event.preventDefault();
                                $state.go('login');
                            }else if( fromState.name != 'base' && fromState.name != "" ){
                                event.preventDefault();
                                $state.go(fromState.name);
                            }else if( fromState.name == "" ){
                                event.preventDefault();
                                $state.go('base');
                            }
                        }

                        //BASE
                        if( toState.name == 'base'){
                            if( typeof $sessionStorage.tokenData == "undefined" ){
                                event.preventDefault();
                                $state.go('login');
                                $rootScope.loadData = false;
                            }else if( fromState.name != 'home' && fromState.name != 'visualisation'
                                && fromState.name != '' && fromState.name != 'facts'&& fromState.name != 'rules'
                                && fromState.name != 'attributes' && fromState.name != 'verification'
                                && fromState.name != 'inference') {
                                event.preventDefault();
                                $state.go(fromState.name);
                                $rootScope.loadData = false;
                            }else if( fromState.name == '' || fromState.name == 'home'){
                                $rootScope.loadData = true;
                            }else{
                                $rootScope.loadData = false;
                            }
                        }

                        //FACTS
                        if( toState.name == 'facts'){
                            if( typeof $sessionStorage.tokenData == "undefined" ){
                                event.preventDefault();
                                $state.go('login');
                            }else if( fromState.name != 'base' && fromState.name != "" ){
                                event.preventDefault();
                                $state.go(fromState.name);
                            }else if( fromState.name == "" ){
                                event.preventDefault();
                                $state.go('base');
                            }
                        }

                        //HOME
                        if( toState.name == 'home'){
                            if( typeof $sessionStorage.tokenData == "undefined" ){
                                event.preventDefault();
                                $state.go('login');
                            }
                        }

                        //INFERENCE
                        if( toState.name == 'inference'){
                            if( typeof $sessionStorage.tokenData == "undefined" ){
                                event.preventDefault();
                                $state.go('login');
                            }else if( fromState.name != 'base' && fromState.name != "" ){
                                event.preventDefault();
                                $state.go(fromState.name);
                            }else if( fromState.name == "" ){
                                event.preventDefault();
                                $state.go('base');
                            }
                        }

                        //LOGIN
                        if( toState.name == 'login'){
                            if( typeof $sessionStorage.tokenData != "undefined" ){
                                event.preventDefault();
                                //$state.go();
                            }
                        }

                        //REGISTRATION
                        if( toState.name == 'registration' ){
                            if( fromState.name != '' || fromState.name != 'login'){
                                //event.preventDefault();
                            }
                        }

                        //RULES
                        if( toState.name == 'rules'){
                            if( typeof $sessionStorage.tokenData == "undefined" ){
                                event.preventDefault();
                                $state.go('login');
                            }else if( fromState.name != 'base' && fromState.name != "" ){
                                event.preventDefault();
                                $state.go(fromState.name);
                            }else if( fromState.name == "" ){
                                event.preventDefault();
                                $state.go('base');
                            }
                        }

                        //VISUALISATION
                        if( toState.name == 'visualisation'){
                            if( typeof $sessionStorage.tokenData == "undefined" ){
                                event.preventDefault();
                                $state.go('login');
                            }else if( fromState.name != 'base' && fromState.name != "" ){
                                event.preventDefault();
                                $state.go(fromState.name);
                            }else if( fromState.name == "" ){
                                event.preventDefault();
                                $state.go('base');
                            }
                        }

                        //VERIFICATION
                        if( toState.name == 'verification'){
                            if( typeof $sessionStorage.tokenData == "undefined" ){
                                event.preventDefault();
                                $state.go('login');
                            }else if( fromState.name != 'base' && fromState.name != "" ){
                                event.preventDefault();
                                $state.go(fromState.name);
                            }else if( fromState.name == "" ){
                                event.preventDefault();
                                $state.go('base');
                            }
                        }

                        if( fromState.name == 'verification' ){
                            for( var i = 0; i < $rootScope.rulesTable.length; i++ ){
                                $rootScope.rulesTable[i][$rootScope.rulesTable[i].length - 1].verification = {
                                    inconsistency: null,
                                    absorption: null,
                                    sameness: null
                                };
                            }
                        }

                    });

                    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams){
                        if( toState.name == 'home'){


                        }
                    });

                    $rootScope.animate = function(event){
                        if( $(event.target).css("left") == "-1px" ){
                            $( event.target ).animate({
                                left: "-=100",
                            }, 500, function() {
                                $(event.target).removeClass("fa-ellipsis-v");
                                $(event.target).addClass("fa-caret-right");
                                $(event.target).parent().children(".sideMenuButton").css("visibility", "inherit");
                            });
                        }else if( $(event.target).css("left") == "-101px" ){
                            $(event.target).parent().children(".sideMenuButton").css("visibility", "hidden");
                            $( event.target ).animate({
                                left: "+=100",
                            }, 500, function() {
                                $(event.target).removeClass("fa-caret-right");
                                $(event.target).addClass("fa-ellipsis-v");
                            });
                        }
                    };

                    $rootScope.animateOff = function( event ){
                        if( $( event.target ).is("i") ){
                            //code for icon
                            $(event.target).parent().parent().children(".sideMenuButton").css("visibility", "hidden");
                            $( event.target).parent().parent().children(".sideMenuDot").animate({
                                left: "+=100",
                            }, 500, function() {
                                $( event.target).parent().parent().children(".sideMenuDot").removeClass("fa-caret-right");
                                $( event.target).parent().parent().children(".sideMenuDot").addClass("fa-ellipsis-v");
                            });
                        }else{
                            $(event.target).parent().children(".sideMenuButton").css("visibility", "hidden");
                            $( event.target).parent().children(".sideMenuDot").animate({
                                left: "+=100",
                            }, 500, function() {
                                $( event.target).parent().children(".sideMenuDot").removeClass("fa-caret-right");
                                $( event.target).parent().children(".sideMenuDot").addClass("fa-ellipsis-v");
                            });
                        }
                    };

                    $rootScope.errorHandler = function( response ){
                        if( response.status == 401 ){

                            if( $rootScope.refreshToken() ){
                                return true;
                            }else{
                                $("#endSessionModal").modal('show');
                            }
                        }else if( response.status == 403 ){
                            $rootScope.tempMsg = "You have not permission";
                            $("#infoModal").modal('show');
                            //modal "You have not permission"
                        }else if( response.status == 404){
                            $rootScope.tempMsg = "Sorry, we couldn't find it";
                            $("#infoModal").modal('show');
                            //modal "Sorry, we couldn't find it."
                        }
                        return false;
                    };

                    $rootScope.refreshToken = function(){
                        return Tokens.refresh().then(function(response){
                            console.log(response.status);

                            if( response.status == 200 ){
                                $sessionStorage.tokenData = response.data;
                                setTimeout(function(){
                                    return true;
                                },2000);
                            }else{
                                return false;
                            }

                        });
                    };

                    $rootScope.logoutUser = function(){
                        $http.post(_APP.urls.apiUrl + '/api/logout').then(function() {
                            $sessionStorage.$reset();
                            $state.go('login');
                        });

                    };

                    /**
                    * Data Tables
                    * */
                    $rootScope.basesTable =[];
                    $rootScope.hashMapB = [];
                    $rootScope.attributesTable = [];
                    $rootScope.hashMap = Object.create(null);
                    $rootScope.rulesTable = [];
                    $rootScope.factsTable = [];
                    $rootScope.hashMapF = Object.create(null);

                    $rootScope.userHasPermission = false;
                    $rootScope.userSharePermission = false;
                    $rootScope.loadData = false;

                }

            ]);


    app.controller('KbExplorerController',[
        '$scope',
        '$rootScope',
        '$http',
        '$location',
        '$state',
        '$sessionStorage',
        'UsersR',
        function( $scope, $rootScope, $http, $location, $state, $sessionStorage, UsersR ){

            $('#endSessionModal').on('hidden.bs.modal', function (e) {
                $rootScope.logoutUser();
            });

            var result = $location.search();
            if( result.success ){
                /**
                 * Logged user data
                 * */

                UsersR.loggedUser().$promise.then(function success(response){
                    $sessionStorage.loggedUser = response;
                    redirect('home');
                }, function error(response){
                    console.log(response);
                });

            }else if( result.error ){
                console.log("error");
            }

            var myApp = this;

            myApp.saveUserChanges = function(){

                var updateData = $rootScope.userInfo;
                updateData.firstName = $scope.userName;
                updateData.lastName = $scope.userSurname;

                console.log(updateData);

                /*UsersR.update(updateData).$promise.then(
                    function success(response){
                        console.log('changes saved');
                    },function error(response){
                        console.log(response);
                    }
                );*/
            };

            function redirect( string ){
                $state.go(string);
            }



        }
    ]);

})();