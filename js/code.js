/**
 * Created by M. on 16.03.2016.
 * * Last edited: 18.07.2016 19:15
 */
$( document ).ready(function() {

    $( window ).resize( function(){
        //var width = $( window ).width();
        var height = $( window).height();
        var headerHeight = 120;
        var footerHeight = 30;
        var param = height - headerHeight - footerHeight + 'px';
        var param2 = height + 'px';
        $(".viewContainer").css("min-height", param);
        $("#baseMask").css("min-height", $(".viewContainer").css("height"));
        $("#verificationMask").css("min-height", $(".viewContainer").css("height"));

    });

    contentHeight();

});

function contentHeight(){
    var height = $( window).height();
    var headerHeight = 120;
    var footerHeight = 30;
    var param = height - headerHeight - footerHeight + 'px';
    var param2 = height + 'px';
    $(".viewContainer").css("min-height", param);
}

function maskHeight(mask){
    $(mask).css("min-height", $(".viewContainer").css("height"));
}

/*function baseMaskHeight(){
    $("#baseMask").css("min-height", $(".viewContainer").css("height"));
}

function verificationMaskHeight(){
    $("#verificationMask").css("min-height", $(".viewContainer").css("height"));
}*/

function samenessComparison( rule1, rule2){
    var samePremises = 0;

    //sprawdzenie d�ugo�ci obu regu�
    if( rule1.length == rule2.length ){
        //sprawdzenie atrybut�w konkluzji
        if( rule1[0].attribute.name == rule2[0].attribute.name ){
            //sprawdzenie warto�ci konkluzji
            if( ( rule1[0].attribute.type == 'continous' && rule1[0].continousValue == rule2[0].continousValue )||
                ( rule1[0].attribute.type != 'continous' && rule1[0].value.name == rule2[0].value.name )
            ){
                //sprawdzenie operator�w konkluzji
                if( rule1[0].operator == rule2[0].operator ){
                    //przegl�d wszystkich przes�anek z 1 regu�y
                    for( var k = 1; k < rule1.length - 1; k ++ ){
                        if( samePremises != k - 1 ) break;
                        //przegl�d wszystkich przes�anek z 2 regu�y
                        for( var l = 1; l < rule2.length - 1; l++ ){
                            //por�wnanie atrybut�w przes�anek
                            if( rule1[k].attribute.name == rule2[l].attribute.name ){
                                //por�wnanie warto�ci przes�anek
                                if( ( rule1[k].attribute.type == 'continous' && rule1[k].continousValue == rule2[l].continousValue )||
                                    ( rule1[k].attribute.type != 'continous' && rule1[k].value.name == rule2[l].value.name )
                                ){
                                    //por�wnanie operator�w przes�anek
                                    if( rule1[k].operator == rule2[l].operator ){
                                        samePremises++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return samePremises;
}

function checkAttributeConflict(conclusionAttribute, premiseAttribute, premises, premiseIndex, editPremiseAttribute){

    var sameAttribute = false, editPremiseConflict = false, attribute = '', attributeTab = [], flag = false;

    if (conclusionAttribute != '' || premiseAttribute != '') {
        if (conclusionAttribute == premiseAttribute){
            sameAttribute = true;
            attribute = conclusionAttribute;
            attributeTab.push(attribute);
        }

        if (premises.length != 0) {
            for (var i = 0; i < premises.length; i++) {
                if (premises[i].attribute.name == conclusionAttribute ||
                    premises[i].attribute.name == premiseAttribute) {
                    sameAttribute = true;
                    attribute = premises[i].attribute.name;

                    flag = false;
                    for (var j = 0; j < attributeTab.length; j++) {
                        if (attributeTab[j] == attribute) flag = true;
                    }
                    if (!flag) attributeTab.push(attribute);
                }
            }
        }
    }

    if( premiseIndex != -1 ){

        if( conclusionAttribute == editPremiseAttribute ||
            premiseAttribute == editPremiseAttribute ){
            sameAttribute = true;
            editPremiseConflict = true;
            attribute = editPremiseAttribute;
            flag = false;
            for( var j = 0; j < attributeTab.length; j++){
                if( attributeTab[j] == attribute ) flag = true;
            }
            if(!flag) attributeTab.push(attribute);
        }

        if( premises.length != 0 ){
            for( var i = 0; i < premises.length; i++ ){
                if( i != premiseIndex ){
                    if( premises[i].attribute.name == editPremiseAttribute ){
                        sameAttribute = true;
                        editPremiseConflict = true;
                        attribute = editPremiseAttribute;
                        flag = false;
                        for( j = 0; j < attributeTab.length; j++ ){
                            if( attributeTab[j] == attribute ) flag = true;
                        }
                        if( !flag ) attributeTab.push(attribute);
                    }
                }
            }
        }

    }

    if(!sameAttribute) attributeTab = [];
    var obj = {
        conflict: sameAttribute,
        editPremiseConflict: editPremiseConflict,
        txt: attributeTab.join(', ')
    };
    return obj;
}