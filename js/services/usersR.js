/**
 * Created by M. on 01.04.2016.
 * Last edit: 10.07.2016 20:10
 */

angular
    .module( 'kbExplorer' ).factory( 'UsersR', function ($resource) {
        return $resource( _APP.urls.apiUrl + '/api/users/:login',
            {
                login: '@login'
            }, {
            'getAll': {method: 'GET', isArray: true},
            'getOne': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'loggedUser': { method: 'GET', url: _APP.urls.apiUrl + '/api/account'},
            'update': { method: 'PUT' }
        });
    });
