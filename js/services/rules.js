/**
 * Created by M. on 06.04.2016.
 * * Last edit: 28.05.2016 12:00
 */
angular
    .module('kbExplorer')
    .factory('Rules', [
        '$http',
        '$sessionStorage',
        function($http, $sessionStorage){
            $http.defaults.headers = {'Accept': 'application/json'};
            var requestUrl;
            //var requestHeader = "Bearer " + $sessionStorage.tokenData.access_token;

            return {
                getAll: function( requestHeader ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/"
                        + $sessionStorage.currentBaseId + "/rules?all=true";

                    return $http.get( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },
                getOne: function( requestHeader, id ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/"
                        + $sessionStorage.currentBaseId + "/rules/" + id;

                    return $http.get( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },
                countAll: function( requestHeader ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/"
                        + $sessionStorage.currentBaseId + "/rules/count";

                    return $http.get( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },
                createFact: function( requestHeader ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/"
                        + $sessionStorage.currentBaseId + "/rules";
                    var data;

                    return $http.post( requestUrl, data, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },
                updateFact: function( requestHeader ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/"
                        + $sessionStorage.currentBaseId +"/rules";
                    var data;

                    return $http.put( requestUrl, data, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },
                deleteFact: function( requestHeader, id ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/"
                        + $sessionStorage.currentBaseId + "/rules/" + id;

                    return $http.delete( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                }
            };
        }
    ]);