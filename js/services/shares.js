/**
 * Created by M. on 16.04.2016.
 * * Last edit: 16.04.2016 15:10
 */
angular
    .module('kbExplorer')
    .factory('Shares', [
        '$http',
        '$sessionStorage',
        function($http, $sessionStorage) {
            $http.defaults.headers = {'Accept': 'application/json'};
            var requestUrl;
            //var requestHeader = "Bearer " + $sessionStorage.tokenData.access_token;

            return {
                getAll: function( requestHeader, id ) {

                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/" + id + "/dbShares";

                    return $http.get( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response);
                        return response;
                    });
                }/*,
                getOne: function(){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/" + $sessionStorage.currentBaseId;

                    return $http.get( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },
                createBase: function(){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases";
                    var data;

                    return $http.post( requestUrl, data, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },
                updateBase: function(){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases";
                    var data;

                    return $http.put( requestUrl, data, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },
                deleteBase: function( ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/" + $sessionStorage.currentBaseId;

                    return $http.delete( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                }*/
            };
        }
    ]);