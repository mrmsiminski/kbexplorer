/**
 * Created by M. on 06.04.2016.
 * * Last edit: 06.06.2016 21:00
 *
 * TO REMOVE
 *
 */
angular
    .module('kbExplorer')
    .factory('Attributes', [
        '$http',
        '$sessionStorage',
        function($http, $sessionStorage){
            $http.defaults.headers = {'Accept': 'application/json'};
            var requestUrl;
            //var requestHeader = "Bearer " + $sessionStorage.tokenData.access_token;

            return {
                getAll: function( requestHeader ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/"
                        + $sessionStorage.currentBaseId + "/attributes?all=true";

                    return $http.get( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },                   //re-implemented
                getOne: function( requestHeader, id ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/"
                        + $sessionStorage.currentBaseId + "/attributes/" + id;

                    return $http.get( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },               //re-implemented
                countAll: function( requestHeader ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/"
                        + $sessionStorage.currentBaseId + "/attributes/count";

                    return $http.get( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },                 //re-implemented
                createAttribute: function( requestHeader, data ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/"
                        + $sessionStorage.currentBaseId + "/attributes";
                    $http.defaults.headers = {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': requestHeader
                    };

                    return $http.post( requestUrl, data ).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },    //re-implemented
                updateAttribute: function( requestHeader ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/"
                        + $sessionStorage.currentBaseId +"/attributes";
                    var data;

                    return $http.put( requestUrl, data, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },          //re-implemented
                deleteAttribute: function( requestHeader, id ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/"
                        + $sessionStorage.currentBaseId + "/attributes/" + id;

                    return $http.delete( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                    }       //re-implemented
            };
        }
    ]);