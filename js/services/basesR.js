/**
 * Created by M. on 01.04.2016.
 * Last edit: 07.06.2016 15:15
 */
angular
    .module( 'kbExplorer' ).factory( 'BasesR', function ( $resource ) {

        return $resource( _APP.urls.apiUrl + '/api/knowledgeBases/:baseId',
            { baseId : '@baseId' }, {
                'getAll': { method: 'GET', isArray: true },
                'getOne': {
                    method: 'GET',
                    transformResponse: function (data) {
                        data = angular.fromJson(data);
                        return data;
                    }
                },
                'update': { method: 'PUT' },
                'create': { method: 'POST' },
                'delete': { method: 'DELETE' }
            });
    });
