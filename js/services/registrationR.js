/**
 * Created by M. on 12.09.2016.
 * Last edit: 25.11.2016 17:10
 */

'use strict';

angular
    .module( 'kbExplorer' ).factory( 'RegistrationR', function ($resource) {
            return $resource( _APP.urls.apiUrl + '/api/register', {}, {
        });
    });
