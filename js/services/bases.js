/**
 * Created by M. on 05.04.2016.
 * * Last edit: 28.05.2016 14:25
 */
angular
    .module('kbExplorer')
    .factory('Bases', [
        '$http',
        '$sessionStorage',
        function( $http, $sessionStorage ) {
            $http.defaults.headers = {'Accept': 'application/json'};
            var requestUrl;
            //var requestHeader = "Bearer " + $sessionStorage.tokenData.access_token;

            return {
                getAll: function( requestHeader ) {

                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases";

                    return $http.get( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response);
                        return response;
                    });
                },
                getOne: function( requestHeader ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/" + $sessionStorage.currentBaseId;

                    return $http.get( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },
                createBase: function( requestHeader, data ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases";
                    $http.defaults.headers = {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': requestHeader
                    };

                    return $http.post( requestUrl, data ).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },
                updateBase: function( requestHeader, data ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases";
                    $http.defaults.headers = {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': requestHeader
                    };

                    return $http.put( requestUrl, data ).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                },
                deleteBase: function( requestHeader, id ){
                    requestUrl = "http://les.szymon.net.pl/api/knowledgeBases/" + id;//$sessionStorage.currentBaseId;

                    return $http.delete( requestUrl, {
                        headers: {'Authorization': requestHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                }
            };
        }
    ])