/**
 * Created by M. on 01.04.2016.
 * Last edit: .06.2016 16:30
 */
angular
    .module( 'kbExplorer' ).factory( 'SharesR', function ( $resource, $sessionStorage ) {

        return $resource( _APP.urls.apiUrl + '/api/knowledgeBases/:baseId/dbShares/:shareId',
            {
                baseId : '@baseId',
                shareId: '@shareId'
            }, {
                'getAll': { method: 'GET', isArray: true },
                'getOne': {
                    method: 'GET',
                    transformResponse: function (data) {
                        data = angular.fromJson(data);
                        return data;
                    }
                },
                'create': { method: 'POST' },
                'update': { method: 'PUT' },
                'delete': { method: 'DELETE' }
            });
    });
