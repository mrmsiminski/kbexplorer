/**
 * Created by M. on 01.04.2016.
 * Last edit: 18.06.2016 13:35
 */
angular
    .module( 'kbExplorer' ).factory( 'FactsR', function ( $resource ) {

        return $resource( _APP.urls.apiUrl + '/api/knowledgeBases/:baseId/facts/:factId',
            {
                baseId : '@baseId',
                factId: '@factId'
            }, {
                'getAll': { method: 'GET', isArray: true,
                    url: _APP.urls.apiUrl + '/api/knowledgeBases/:baseId/facts?all=true'},
                'getOne': {
                    method: 'GET',
                    transformResponse: function (data) {
                        data = angular.fromJson(data);
                        return data;
                    }
                },
                'countAll':  { method: 'GET',
                    url: _APP.urls.apiUrl + "/api/knowledgeBases/:baseId/facts/count"
                },
                'create': { method: 'POST' },
                'update': { method: 'PUT' },
                'delete': { method: 'DELETE' }
            });
    });
