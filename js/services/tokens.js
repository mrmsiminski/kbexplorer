/**
 * Created by M. on 15.04.2016.
 * * Last edit: 15.04.2016 17:20
 */

angular
    .module('kbExplorer')
    .factory('Tokens', [
        '$http',
        '$sessionStorage',
        function($http, $sessionStorage) {
            $http.defaults.headers = {'Accept': 'application/json'};
            var requestUrl;
            //var requestHeader = "Bearer " + $sessionStorage.tokenData.access_token;

            return {
                refresh: function () {
                    var temp = {};
                    temp.status = 202;
                    requestUrl = "http://les.szymon.net.pl/oauth/token?grant_type=refresh_token&refresh_token="
                                + $sessionStorage.tokenData.refresh_token;
                    var authHeader = window.btoa("KbExplorer:mgrPass");

                    return $http.post(requestUrl, "", {
                        headers: {'Authorization': 'Basic ' + authHeader}
                    }).then(function successCallback(response) {
                        return response;

                    }, function errorCallback(response) {
                        console.log(response.status);
                        return response;
                    });
                }
            }
        }
    ]);