/**
 * Created by M. on 01.04.2016.
 * Last edit: 07.06.2016 16:30
 */
angular
    .module( 'kbExplorer' ).factory( 'RulesR', function ( $resource, $sessionStorage ) {

        return $resource( _APP.urls.apiUrl + '/api/knowledgeBases/:baseId/rules/:ruleId',
            {
                baseId : '@baseId',
                ruleId: '@ruleId'
            }, {
                'getAll': { method: 'GET', isArray: true,
                    url: _APP.urls.apiUrl + '/api/knowledgeBases/:baseId/rules?all=true'},
                'getOne': {
                    method: 'GET',
                    transformResponse: function (data) {
                        data = angular.fromJson(data);
                        return data;
                    }
                },
                'countAll':  { method: 'GET',
                    url: _APP.urls.apiUrl + "/api/knowledgeBases/:baseId/rules/count"
                },
                'create': { method: 'POST' },
                'update': { method: 'PUT' },
                'delete': { method: 'DELETE' }
            });
    });
