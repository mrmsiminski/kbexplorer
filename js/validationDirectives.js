/**
 * Created by M. on 07.06.2016.
 * Last edit: 07.06.2016 18:00
 */

angular
    .module( 'kbExplorer' ).directive('baseNameDirective', function($q, $timeout, $rootScope) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                var names = [];

                function fun() {
                    names = [];
                    for (var i = 0; i < $rootScope.basesTable.length; i++) {
                        names.push($rootScope.basesTable[i].name);
                    }
                }

                ctrl.$asyncValidators.baseNameDirective = function(modelValue, viewValue) {

                    if( names.length != $rootScope.basesTable.length ) fun();

                    if (ctrl.$isEmpty(modelValue)) {
                        // pusty 'model' jest poprawny
                        return $q.when();
                    }

                    var def = $q.defer();

                    $timeout(function() {
                        // op�nienie odpowiedzi
                        if (names.indexOf(modelValue) === -1 ||
                            modelValue == scope.home.currentBase.name && scope.home.creation == 0 ) {
                            // nazwa bazy dost�pna
                            def.resolve();
                        } else {
                            def.reject();
                        }

                    }, 2000);

                    return def.promise;
                };
            }
        };
    });

angular
    .module( 'kbExplorer' ).directive('baseNameDirective2', function($q, $timeout, $rootScope) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                var names = [];
                fun();

                function fun() {
                    names = [];
                    for (var i = 0; i < $rootScope.basesTable.length; i++) {
                        names.push($rootScope.basesTable[i].name);
                    }
                }

                ctrl.$asyncValidators.baseNameDirective2 = function(modelValue, viewValue) {

                    if( names.length != $rootScope.basesTable.length ) fun();

                    if (ctrl.$isEmpty(modelValue)) {
                        // consider empty model valid
                        return $q.when();
                    }

                    var def = $q.defer();

                    $timeout(function() {
                        // Mock a delayed response
                        if (names.indexOf(modelValue) === -1 ||
                            modelValue == scope.$baseData.name ) {
                            // The username is available
                            def.resolve();
                        } else {
                            def.reject();
                        }

                    }, 2000);

                    return def.promise;
                };
            }
        };
    });


angular
    .module( 'kbExplorer' ).directive('attributeNameDirective', function($q, $timeout, $rootScope) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                var names = [];

                function fun() {
                    names = [];
                    for (var i = 0; i < $rootScope.attributesTable.length; i++) {
                        names.push($rootScope.attributesTable[i].name);
                    }
                }

                ctrl.$asyncValidators.attributeNameDirective = function(modelValue, viewValue) {

                    if( names.length != $rootScope.attributesTable.length ) fun();

                    if (ctrl.$isEmpty(modelValue)) {
                        // consider empty model valid
                        return $q.when();
                    }

                    var def = $q.defer();

                    $timeout(function() {
                        // Mock a delayed response
                        if (names.indexOf(modelValue) === -1 ||
                            modelValue == scope.attributes.currentAttribute.name && scope.attributes.editing == true ) {
                            // The username is available
                            def.resolve();
                        } else {
                            def.reject();
                        }

                    }, 2000);

                    return def.promise;
                };
            }
        };
    });

angular
    .module( 'kbExplorer' ).directive('valueNameDirective', function($q, $timeout, $rootScope) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                var names = [];

                function fun() {
                    names = [];
                    for (var i = 0; i < scope.attributes.valuesArray.length; i++) {
                        names.push(scope.attributes.valuesArray[i].name);
                    }
                }

                ctrl.$asyncValidators.valueNameDirective = function(modelValue, viewValue) {

                    if ( names.length != scope.attributes.valuesArray.length ){
                        fun();
                    }

                    if (ctrl.$isEmpty(modelValue)) {
                        // consider empty model valid
                        return $q.when();
                    }

                    var def = $q.defer();

                    $timeout(function() {

                        if (names.indexOf(modelValue) === -1 ||
                            scope.attributes.editingValue != -1 && scope.attributes.valuesArray[scope.attributes.editingValue].name == modelValue ) {
                            // The username is available
                            def.resolve();
                        } else {
                            def.reject();
                        }

                    }, 2000);

                    return def.promise;
                };
            }
        };
    });

angular
    .module( 'kbExplorer' ).directive('passwordDirective', function($q, $timeout, $rootScope) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                var password = "";
                var confirmationPassword = "";

                ctrl.$asyncValidators.passwordDirective = function(modelValue, viewValue) {

                    password = scope.password;
                    confirmationPassword = scope.confirmPassword;

                    if (ctrl.$isEmpty(modelValue)) {
                        // consider empty model valid
                        return $q.when();
                    }

                    var def = $q.defer();

                    $timeout(function() {

                        if ( password == modelValue ) {
                            // The username is available
                            def.resolve();
                        } else {
                            def.reject();
                        }

                    }, 2000);

                    return def.promise;
                };
            }
        };
    });

// username validation directive