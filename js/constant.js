/**
 * Created by M. on 01.04.2016.
 * Last edit: 06.06.2016 23:15
 */

const _APP = {
    auth: {
        authHeader: window.btoa("KbExplorer:mgrPass"),
        requestUrl:'http://les.szymon.net.pl/oauth/token?grant_type=authorization_code&code=',
        redirectUrl:'&redirect_uri=http://www.kbexplorer.ii.us.edu.pl/~msiminski/assets/auth.html',
        refreshUrl: "http://les.szymon.net.pl/oauth/token?grant_type=refresh_token&refresh_token="
    },
    urls: {
        requestUrl:'http://les.szymon.net.pl/oauth/authorize?response_type=code&client_id=KbExplorer',
        apiUrl: 'http://les.szymon.net.pl'
    }
};