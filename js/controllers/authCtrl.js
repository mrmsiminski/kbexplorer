/**
 * Created by M. on 31.03.2016.
 * Last edit: 31.03.2016 15:00
 */

angular
    .module('kbExplorer')
    .controller('authCtrl', [
        '$scope',
        '$http',
        '$location',
        function($scope, $http, $location) {

            $http.defaults.headers.post = {'Accept': 'application/json'};
            var authHeader = window.btoa("KbExplorer: mgrPass");
            var requestUrl = "http://les.szymon.net.pl/oauth/token?grant_type=authorization_code&code=";
            var redirectUrl = "&redirect_uri=http://www.kbexplorer.ii.us.edu.pl/~msiminski/#/auth";

            var authCode = $location.search();
            $scope.result = authCode.code;
            console.log(authCode);

            /*$http.post(requestUrl + authCode.code + redirectUrl, {
                headers: {'Authorization': 'Basic ' + authHeader}
            })
            .then(function successCallback(response) {
                console.log(response);
                alert("oks");
            }, function errorCallback(response) {
                console.log(response);
                //save();
            });*/

        }
    ]);
