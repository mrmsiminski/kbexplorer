/**
 * Created by M. on 01.04.2016.
 * Last edit: 18.07.2016 19:15
 */

angular
    .module('kbExplorer')
    .controller('baseCtrl', [
        '$scope',
        '$rootScope',
        '$http',
        '$location',
        '$sessionStorage',
        '$window',
        '$state',
        'BasesR',
        'AttributesR',
        'FactsR',
        'RulesR',
        'SharesR',
        'UsersR',
        '$timeout',
        function($scope, $rootScope, $http, $location, $sessionStorage, $window, $state, BasesR, AttributesR, FactsR,
                 RulesR, SharesR, UsersR, $timeout) {

            contentHeight();
            maskHeight("#baseMask");

            var basePage = this;
            basePage.currentBase = {};
            basePage.shareEditing = false;
            basePage.shareExists = false;
            basePage.permission = $rootScope.userHasPermission;
            basePage.sharePermission = false;
            basePage.maskShow = $rootScope.loadData;

            /**
             * Creating baseTable if necessary
             * */

            if( $rootScope.basesTable.length == 0 ){
                BasesR.getAll().$promise.then(function success(response){
                    for( var i = 0; i < response.length; i++ ){
                        response[i].tabPosition = i;
                        $rootScope.basesTable[i] = response[i];
                        $rootScope.hashMapB[response[i].id] = $rootScope.basesTable[i];
                    }
                    basePage.currentBase = $rootScope.hashMapB[$sessionStorage.currentBaseId];
                }, function error(response){
                    console.log(response);
                });
            }

            /**
             * Loading base data
             * */
            //ca�o�ciowy czas
            var allTimeStart, allTimeEnd, allRealTime;
            //pobranie atrybut�w
            var dAttrStart, dAttrEnd, dAttrReal;
            //mapowanie atrybut�w
            var mAttrStart, mAttrEnd, mAttrReal;
            //pobranie fakt�w
            var dFactStart, dFactEnd, dFactReal;
            //mapowanie fakt�w
            var mFactStart, mFactEnd, mFactReal;
            //pobranie regu�
            var dRuleStart, dRuleEnd, dRuleReal;
            //mapowanie regu�
            var mRuleStart, mRuleEnd, mRuleReal;

            if( $rootScope.loadData ){

                loadBaseData(function(){

                    $scope.numF = $rootScope.factsTable.length;
                    $scope.numR = $rootScope.rulesTable.length;
                    $scope.numA = $rootScope.attributesTable.length;
                    basePage.sharePermission = shareValidation();
                    $rootScope.loadData = false;
                    basePage.maskShow = $rootScope.loadData;

                    //$timeout(function(){
                        dAttrReal = dAttrEnd - dAttrStart;
                        mAttrReal = mAttrEnd - mAttrStart;
                        dFactReal = dFactEnd - dFactStart;
                        mFactReal = mFactEnd - mFactStart;
                        dRuleReal = dRuleEnd - dRuleStart;
                        mRuleReal = mRuleEnd - mRuleStart;
                        allRealTime = allTimeEnd - allTimeStart;

                        var factT = dFactReal + mFactReal;
                        var attrT = dAttrReal + mAttrReal;
                        var ruleT = dRuleReal + mRuleReal;

                        //console.log('Pobieranie atrybutow: ' + dAttrReal + ' ms' );
                        //console.log('Mapowanie atrybutow: ' + mAttrReal + ' ms' );
                        //console.log('Pobieranie faktow: ' + dFactReal + ' ms' );
                        //console.log('Mapowanie faktow: ' + mFactReal + ' ms' );
                        //console.log('Pobieranie regul: ' + dRuleReal + ' ms' );
                        //console.log('Mapowanie regul: ' + mRuleReal + ' ms' );
                        //console.log('Atrybuty:' + attrT + 'ms');
                        //console.log('Fakty:' + factT + 'ms');
                        //console.log('Reguly:' + ruleT + 'ms');
                        //console.log('Calosciowy czas: ' + allRealTime + ' ms' );
                    //},1500);

                });


            }else{
                getShares();
                basePage.sharePermission = shareValidation();
                $scope.numF = $rootScope.factsTable.length;
                $scope.numR = $rootScope.rulesTable.length;
                $scope.numA = $rootScope.attributesTable.length;
            }


            function loadBaseData(callback){
                allTimeStart = performance.now();
                dAttrStart = performance.now();

                var dfrd1 = $.Deferred();
                var dfrd2= $.Deferred();
                var dfrd3 = $.Deferred();
                var dfrd4= $.Deferred();

                AttributesR.getAll({baseId: $sessionStorage.currentBaseId}).$promise.then(function success(response){
                    dAttrEnd = performance.now();

                    createAttributesTable(response).done(function(){dfrd1.resolve();});
                    loadFacts().done(function(){dfrd2.resolve();});
                    loadRules().done(function(){dfrd3.resolve();});
                    getShares(function(){
                            basePage.validateUserPermission();
                            basePage.permission = $rootScope.userHasPermission;
                            dfrd4.resolve();
                            //console.log('4');
                        }
                    );

                }, function error(response){
                    console.log(response);
                });

                return $.when(dfrd1, dfrd2,dfrd3, dfrd4).done(function(){
                    allTimeEnd = performance.now();
                    callback();
                }).promise();
            }

            /**
             * Counting elements of base
             * */
            /*$scope.numF = $rootScope.factsTable.length;
            $scope.numR = $rootScope.rulesTable.length;
            $scope.numA = $rootScope.attributesTable.length;*/

            /**
             * Getting users with access to base
             * */

            function getShares(callback){
                basePage.currentBase = $rootScope.hashMapB[$sessionStorage.currentBaseId];
                $scope.$baseData = basePage.currentBase;

                $timeout(function(){
                    SharesR.getAll({baseId: $sessionStorage.currentBaseId}).$promise.then(function success(response){
                        $scope.shares = response;
                        if(callback){
                            callback();
                        }
                        return true;
                    }, function error(response){
                        console.log(response);
                        return false;
                    });
                },100);
            }

            /**
             * Table of attributes and hashMap
             **/

            function createAttributesTable( data ){
                mAttrStart = performance.now();
                var key, value;
                var deferred = $.Deferred();
                $rootScope.attributesTable = [];
                for( var i = 0; i < data.length; i++ ){
                    var obj = {
                        tabPosition: i,
                        id: data[i].id,
                        name: data[i].name,
                        description: data[i].description,
                        type: data[i].type,
                        isFact: data[i].isFact,
                        query: data[i].query,
                        values: data[i].values
                    };

                    $rootScope.attributesTable.push( obj );
                    key = obj.id;
                    value = $rootScope.attributesTable[$rootScope.attributesTable.length -1];
                    $rootScope.hashMap[ key ] = value;

                    if( i == data.length - 1 ){
                        mAttrEnd = performance.now();
                        deferred.resolve();
                    }

                }

                if( data.length == 0 ){
                    mAttrEnd = performance.now();
                    deferred.resolve();
                }

                return $.when(deferred).promise();
            }

            /**
             * Table of rules
             **/

            function loadRules(){
                var deferred = $.Deferred();
                dRuleStart = performance.now();
                RulesR.getAll( {baseId: $sessionStorage.currentBaseId}).$promise.then( function success(response){
                    dRuleEnd = performance.now();
                    createRulesTable( response).then(function(){deferred.resolve();});
                }, function error(response){
                    console.log(response);
                });
                return $.when(deferred).promise();
            }

            function createRulesTable( data ){
                var deferred = $.Deferred();
                mRuleStart = performance.now();
                $rootScope.rulesTable = [];
                for( var i = 0; i < data.length; i++ ){
                    var elements = data[i].attributeValues;
                    var counter = 1;

                    if( typeof $rootScope.rulesTable[i] == 'undefined' ){
                        $rootScope.rulesTable[i] = new Array();
                    }

                    for( var j = 0; j < elements.length; j++ ){

                        var elem = elements[j];

                        if( typeof elem != 'undefined' && elem.isConclusion == true ){
                            $rootScope.rulesTable[i][0] = elements[j];
                        }else if( typeof  elem != 'undefined' && elem.attributeOrder != 0 ){
                            $rootScope.rulesTable[i][elements[j].attributeOrder] = elements[j];
                        }else if( typeof elem != 'undefined' ){
                            $rootScope.rulesTable[i][counter] = elements[j];
                            counter++
                        }

                    }

                    var rule = $rootScope.rulesTable[i];
                    for( j = 0; j < rule.length; j++ ){
                        if( typeof rule[j] != 'undefined' ){
                            rule[j].attributeOrder = j + 1;
                        }else{
                            rule.splice(j, 1);
                            /*for( var s = j + 1; s < rule.length; s++ ){
                                rule[s-1] = rule[s];
                                if(s == rule.length -1 ) rule.splice(s,1);
                            }
                            j--;*/
                        }

                    }

                    var obj = {
                        ruleId: data[i].id,
                        index: i,
                        description: data[i].description,
                        verification:{
                            inconsistency: null,
                            absorption: null,
                            sameness: null
                        }
                    };

                    $rootScope.rulesTable[i].push(obj);

                    if( i == data.length - 1 ){
                        mRuleEnd = performance.now();
                        deferred.resolve();
                    }
                }

                if( data.length == 0 ){
                    mRuleEnd = performance.now();
                    deferred.resolve();
                }

                return $.when(deferred).promise();
            }

            /**
            * Table of facts
            * */

            function loadFacts() {
                dFactStart = performance.now();
                var deferred = $.Deferred();
                FactsR.getAll({baseId: $sessionStorage.currentBaseId}).$promise.then(function success(response) {
                    dFactEnd = performance.now();
                    createFactsTable(response).done(function(){ deferred.resolve(); });
                }, function error(response) {
                    console.log(response);
                });

                return $.when(deferred).promise();
            }

            function createFactsTable( data ){
                mFactStart = performance.now();
                var deferred = $.Deferred();
                var obj;
                var key, value;
                $rootScope.factsTable = [];
                for( var i = 0; i < data.length; i++ ){
                    obj = {
                        id: data[i].id,
                        attribute: $rootScope.hashMap[ data[i].attribute.id ],
                        operator: data[i].operator,
                        value: data[i].value,
                        continousValue: data[i].continousValue,
                        tabPosition: i
                    };
                    $rootScope.factsTable.push( obj );
                    key = obj.id;
                    value = $rootScope.factsTable[ $rootScope.factsTable.length -1 ];
                    $rootScope.hashMapF[key] = value;

                    if( i == data.length - 1 ){
                        mFactEnd = performance.now();
                        deferred.resolve();
                    }
                }

                if( data.length == 0 ){
                    deferred.resolve();
                    mFactEnd = performance.now();
                }

                return $.when(deferred).promise();
            }

            /**
             * Base editing and deleting
             * */

            basePage.editModal = function(){
                $scope.baseName = basePage.currentBase.name;
                $scope.baseDescription = basePage.currentBase.description;
                $("#baseModal").modal('show');
            };

            basePage.editBase = function(){
                var index = $scope.$baseData.tabPosition;
                basePage.currentBase.name = $scope.baseName;
                basePage.currentBase.description = $scope.baseDescription;

                BasesR.update(basePage.currentBase).$promise.then(function success(response){
                    $rootScope.basesTable[index] = basePage.currentBase;
                    $("#baseModal").modal('hide');
                }, function error(response){
                    console.log(response);
                });
            };

            basePage.deleteInfo = function(){
                $scope.deletePositionName = basePage.currentBase.name;
                $('#deleteModal').modal('show')
            };

            basePage.deleteBase = function(){
                var index = basePage.currentBase.tabPosition;
                BasesR.delete( {baseId: $sessionStorage.currentBaseId}).$promise.then(function success(response){
                    $rootScope.basesTable.splice( index, 1 );
                    delete $rootScope.hashMapB[$sessionStorage.currentBaseId];
                    $state.go('home');
                }, function error(response){
                    console.log(response);
                });
            };

            /**
             * Share permission validation
             * */

            function shareValidation(){
                var user = $sessionStorage.loggedUser;
                var base = $rootScope.hashMapB[$sessionStorage.currentBaseId];
                var permission;

                //sprawdzenie, czy Admin
                for( var i = 0; i < user.authorities.length; i++ ){
                    if( user.authorities[i] == "ROLE_ADMIN" ){
                        permission = true;
                        return permission;
                    }else{
                        permission = false;
                    }
                }

                //sprawdzenie, czy jest Ownerem
                if( user.login == base.owner.login ){
                    permission = true;
                }else{
                    permission = false;
                }

                return permission;
            }

            /**
             * New base sharing
             * */

            basePage.shareModal = function ( edit, id ){

                basePage.setModal(edit, id);

            };

            basePage.setModal = function( edit, id ){
                $scope.sharePermissionsSelect = {};
                $scope.shareUsersSelect = {};
                basePage.shareEditing = false;

                UsersR.getAll().$promise.then(function success(response){
                    $scope.users = response;
                    $scope.sharePermissions = [
                        {data: 'readOnly', name: 'Read only'},
                        {data: 'full', name: 'Full'}
                    ];
                }, function error(response){
                    console.log(response);
                });

                $scope.shareModalTitle = 'Creating';
                if( edit ){
                    basePage.shareEditing = true;
                    $scope.shareModalTitle = 'Editing';
                    $scope.currentShareId = id;
                    SharesR.getOne({baseId: $sessionStorage.currentBaseId, shareId: id}).$promise.then(
                        function success(response){
                            var permission;
                            if( response.permision == 'full'){
                                permission = {
                                    data: response.permision,
                                    name: 'Full'
                                };
                            }else{
                                permission = {
                                    data: response.permision,
                                    name: 'Read only'
                                };
                            }

                            $timeout(function(){
                                $scope.sharePermissionsSelect.selected = permission;
                                $scope.shareUsersSelect.selected = response.user;
                            },100);
                        },
                        function error(response){
                            console.log(response);
                        }
                    );
                }

                $("#shareModal").modal('show');
            };

            basePage.createShare = function (){

                var createData = {
                    "id": null,
                    "knowledgeBase": basePage.currentBase,
                    "permision": $scope.sharePermissionsSelect.selected.data,
                    "user": $scope.shareUsersSelect.selected,
                    baseId: $sessionStorage.currentBaseId
                };

                SharesR.create(createData).$promise.then(function success(response){
                    getShares();
                    $("#shareModal").modal('hide');
                }, function error(response){
                    console.log(response);
                });
            };

            /**
             * Editing base sharing
             * */

            basePage.editShare = function( event, edit, id ){
                $rootScope.animateOff(event);//basePage.animate(event);
                basePage.shareModal( edit, id );
            };

            basePage.saveShareChanges = function(id){

                var updateData = {
                    "id": id,
                    "knowledgeBase": basePage.currentBase,
                    "permision": $scope.sharePermissionsSelect.selected.data,
                    "user": $scope.shareUsersSelect.selected,
                    baseId: $sessionStorage.currentBaseId
                };
                SharesR.update(updateData).$promise.then(
                    function success(response){
                        getShares();
                        $("#shareModal").modal('hide');
                    },
                    function error(response){
                        console.log(response);
                    }
                );
            };

            /**
             * Deleting base share
             * */

            basePage.deleteShareInfo = function( event, id, user ){
                basePage.animate(event);
                $scope.shareUserName = user;
                $scope.deleteShareId = id;

                $("#deleteShareModal").modal('show');
            };

            basePage.deleteShare = function(id){
                SharesR.delete({baseId: $sessionStorage.currentBaseId, shareId: id}).$promise.then(
                    function success(response){
                        getShares();
                    },
                    function error(response){
                        console.log(response);
                    }
                );
            };

            /**
             * Validation base sharing
             * */

            $('#shareModal').on('hidden.bs.modal', function (e) {
                basePage.shareExists = false;
            });


            basePage.validateShare = function( user ){

                for( var i = 0; i < $scope.shares.length; i++ ){
                    if( $scope.shares[i].user.login == user.login ){
                        basePage.shareExists = true;
                        break;
                    }else{
                        basePage.shareExists = false;
                    }

                }
            };

            /**
             * Validation user permission
             * */

            basePage.validateUserPermission = function(){
                var user = $sessionStorage.loggedUser;
                //sprawdzenie, czy Admin
                for( var i = 0; i < user.authorities.length; i++ ){
                    if( user.authorities[i] == "ROLE_ADMIN" ){
                        $rootScope.userHasPermission = true;
                        return $rootScope.userHasPermission;
                    }else{
                        $rootScope.userHasPermission = false;
                    }
                }

                //sprawdzenie, czy jest Ownerem
                if( user.login == basePage.currentBase.owner.login ){
                    $rootScope.userHasPermission = true;
                    return $rootScope.userHasPermission;
                }else{
                    $rootScope.userHasPermission = false;
                }

                //sprawdzenie, czy posiada permission 'full'
                for( var i = 0; i < $scope.shares.length; i ++ ){
                    if( $scope.shares[i].user.login == user.login &&
                        $scope.shares[i].permision == 'full' ){
                        $rootScope.userHasPermission = true;
                        return $rootScope.userHasPermission;
                    }else{
                        $rootScope.userHasPermission = false;
                    }
                }
                return $rootScope.userHasPermission;
            };

            /**
             * Navigation and functionality
             * */

            basePage.visualizeIt = function(){
                $state.go('visualisation');
            };

            basePage.showAttributes = function(){
                $state.go('attributes');
            };

            basePage.showFacts = function(){
                $state.go('facts');
            };

            basePage.showRules = function(){
                $state.go('rules');
            };

            basePage.test = function(){
                console.log("sth happened");
            };

            basePage.animate = function( event ){
                $rootScope.animate( event );
            };

        }
    ]);
