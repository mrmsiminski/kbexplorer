/**
 * Created by M. on 07.04.2016.
 * Last edit: 18.07.2016 19:30
 */

angular
    .module('kbExplorer')
    .controller('attributesCtrl', [
        '$state',
        '$scope',
        '$rootScope',
        '$sessionStorage',
        'AttributesR',
        '$timeout',
        function($state, $scope, $rootScope, $sessionStorage, AttributesR, $timeout){

            contentHeight();
            var attributesPage = this;
            attributesPage.editing = false;
            attributesPage.creating = false;
            attributesPage.showForm = false;
            attributesPage.editingVal = false;
            attributesPage.editingValue = -1;
            attributesPage.permission = $rootScope.userHasPermission;//$sessionStorage.permission;

            attributesPage.baseId = $sessionStorage.currentBaseId;
            attributesPage.attributeId = "";

            attributesPage.currentAttribute = {};
            attributesPage.valuesArray = [];

            attributesPage.tempAttribute = {};
            $scope.attributeTypeSelect = {};

            $scope.editing = attributesPage.editing;
            $scope.attribute = attributesPage.currentAttribute;
            $scope.items = $rootScope.attributesTable;

            /**
             * Create new attribute
             * */

            attributesPage.createNewAttribute = function(){

                var query = null;
                if( $scope.attributeFormQuery != '' ) query = $scope.attributeFormQuery;

                var data = {
                    "description": $scope.attributeFormDescription,
                    "id": null,
                    "internalDescription": null,
                    "isSingleFact": $scope.attributeFormFact,
                    "name": $scope.attributeFormName,
                    "query": query,
                    "type": $scope.attributeTypeSelect.selected.name,
                    "values": attributesPage.valuesArray,
                    "baseId": $sessionStorage.currentBaseId
                };

                AttributesR.create( data ).$promise.then(function success(response){

                    data.id = response.id;
                    data.tabPosition = $rootScope.attributesTable.length;
                    delete data.baseId;

                    $rootScope.attributesTable.push( data );

                    var key = data.id;
                    var value = $rootScope.attributesTable[data.tabPosition];
                    $rootScope.hashMap[ key ] = value;

                }, function error(response){
                    console.log(response);
                });
            };

            /**
             * Edit attribute
             * */

            attributesPage.editAttribute = function( id, event ){
                $rootScope.animateOff( event );
                attributesPage.openModal( id, 2 );
            };

            attributesPage.saveChanges = function( id ){

                attributesPage.currentAttribute.name = $scope.attributeFormName;
                attributesPage.currentAttribute.type = $scope.attributeTypeSelect.selected.name;
                attributesPage.currentAttribute.isSingleFact = $scope.attributeFormFact;
                attributesPage.currentAttribute.description = $scope.attributeFormDescription;
                attributesPage.currentAttribute.values = attributesPage.valuesArray;
                attributesPage.currentAttribute.baseId = $sessionStorage.currentBaseId;
                attributesPage.currentAttribute.id = id;
                attributesPage.currentAttribute.query = $scope.attributeFormQuery;

                //console.log(attributesPage.currentAttribute.isSingleFact);

                if( $scope.attributeTypeSelect.selected.name == 'continous') attributesPage.currentAttribute.values = null;

                AttributesR.update( attributesPage.currentAttribute ).$promise.then(function success(response){

                    delete attributesPage.currentAttribute.baseId;
                    $rootScope.attributesTable[attributesPage.currentAttribute.tabPosition] = attributesPage.currentAttribute;

                }, function error(response){
                    console.log(response);
                });

            };

            /**
             * Deleting attribute
             * */

            attributesPage.deleteInfo = function( id, event ){
                $rootScope.animateOff( event );
                $scope.deleteAttributeId = id;
                $scope.deleteAttributeName = $rootScope.hashMap[id].name;
                $("#deleteModal").modal("show");
            };

            attributesPage.deleteAttribute = function( id ){

                AttributesR.delete({baseId: $sessionStorage.currentBaseId, attributeId: id}).$promise.then(function success(response){
                    var index = $rootScope.attributesTable.indexOf( $rootScope.hashMap[id] );
                    $rootScope.attributesTable.splice(index, 1);
                    delete $rootScope.hashMap[id];
                }, function error(response){
                    console.log( response );
                });

            };

            attributesPage.animate = function( event ){
                $rootScope.animate( event );
            };

            /**
             * Modal functions
             * */

            $('#attributeModal').on('hidden.bs.modal', function (e) {
                attributesPage.clearForm();
                attributesPage.editingValue = -1;
                attributesPage.editingVal = false;
            });

            $('#deleteValueModal').on('hidden.bs.modal', function (e) {
                $("body").addClass('modal-open');
            });

            attributesPage.clearForm = function(){
                $scope.attributeFormName = "";
                $scope.attributeTypeSelect.selected = "";
                $scope.attributeFormQuery = "";
                $scope.attributeFormFact = false;
                $scope.attributeFormDescription = "";
                $scope.values = [];

            };

            /*attributesPage.readToEdit = function(){
                attributesPage.editing = true;
                setModal( 2 );
            };*/

            attributesPage.openModal = function( id, mode ){
                $scope.types = [{name: 'symbolic'}, {name: 'continous'}, {name: 'discrete'}];
                if( id != -1 ){
                    attributesPage.currentAttribute = $rootScope.hashMap[id];
                    setModal( mode );
                    $("#attributeModal").modal('show');
                }else{
                    setModal( mode );
                    $("#attributeModal").modal('show');
                }

            };

            function setModal( mode ){
                switch ( mode ) {
                    case 1:
                        attributesPage.editing = false;
                        attributesPage.creating = false; //readOnly

                        $scope.attributeModalTitle = "Attribute";
                        $scope.attributeReadName = attributesPage.currentAttribute.name;
                        $scope.attributeReadType = attributesPage.currentAttribute.type;
                        $scope.attributeReadFact = attributesPage.currentAttribute.isSingleFact;
                        $scope.attributeReadDescription = attributesPage.currentAttribute.description;
                        $scope.attributeReadQuery = attributesPage.currentAttribute.query;

                        if( attributesPage.currentAttribute.isSingleFact == null )
                            $scope.attributeReadFact = false;
                        if( attributesPage.currentAttribute.type != "continous" ){
                            $scope.values = attributesPage.currentAttribute.values;//attributesPage.tempAttribute.values;
                        }

                        break;
                    case 2:
                        attributesPage.creating = false; //editing
                        attributesPage.editing = true;

                        $scope.attributeModalTitle = "Editing";
                        $scope.attributeFormName = attributesPage.currentAttribute.name;
                        $scope.attributeFormFact = attributesPage.currentAttribute.isSingleFact;
                        $scope.attributeFormDescription = attributesPage.currentAttribute.description;
                        $scope.attributeId = attributesPage.currentAttribute.id;
                        $scope.attributeFormQuery = attributesPage.currentAttribute.query;

                        if( attributesPage.currentAttribute.type != "continous" ){
                            attributesPage.valuesArray = attributesPage.currentAttribute.values;
                            $scope.values = attributesPage.valuesArray;
                        }

                        $timeout(function(){
                            $scope.attributeTypeSelect.selected = {name: attributesPage.currentAttribute.type};
                        },100);

                        break;
                    case 3:
                        attributesPage.editing = false;
                        attributesPage.creating = true; //creating

                        $scope.attributeModalTitle = "Creating";
                        $scope.attributeFormName = "";
                        //$scope.attributeFormType = "symbolic";
                        $scope.attributeFormFact = null;
                        $scope.attributeFormDescription = "";

                        attributesPage.valuesArray = [];
                        $scope.values = attributesPage.valuesArray;

                        break;
                }
            }

            /**
             * Adding new value
             * */

            attributesPage.clearValueForm = function(){
                $scope.newValueName = "";
                $scope.newValueDescription = "";
            };

            attributesPage.addValue = function(){
                var obj = {
                    name: $scope.newValueName,
                    description: $scope.newValueDescription
                };
                attributesPage.valuesArray.push( obj );
                attributesPage.clearValueForm();
            };

            /**
             * Editing value
             * */

            attributesPage.editAttributeValue = function( id, event ){
                $rootScope.animateOff( event );
                attributesPage.editingValue = id;
                attributesPage.editingVal = true;

                $scope.valueEditName = attributesPage.valuesArray[id].name;
                if( attributesPage.valuesArray[id].description == null ){
                    $scope.valueEditDescription = "";
                }else{
                    $scope.valueEditDescription = attributesPage.valuesArray[id].description;
                }

            };

            attributesPage.changeAttributeValue = function( id ){
                var obj = {
                    name: $("#fni"+id).val(),
                    description: $("#fdi"+id).val()
                };
                attributesPage.valuesArray[id] = obj;
                attributesPage.editingValue = -1;
                attributesPage.editingVal = false;
            };

            /**
             * Deleting values
             * */

            attributesPage.deleteAttributeValueInfo = function( index, event ){
                $rootScope.animateOff( event );
                $scope.deleteValueName = $scope.values[index].name;//setting value name in modal
                $scope.deleteValueIndex = index;//seting index as param
                $("#deleteValueModal").modal('show');//showing modal
            };

            attributesPage.deleteAttributeValue = function( index ){
                attributesPage.valuesArray.splice( index, 1 );
            };

            /**
             * Other
             * */

            attributesPage.test = function( type ){
                console.log(type);
            };

        }
    ]);