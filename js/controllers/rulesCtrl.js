/**
 * Created by M. on 07.04.2016.
 * Last edit: 18.07.2016 19:10
 */

angular
    .module('kbExplorer')
    .controller('rulesCtrl', [
        '$state',
        '$scope',
        '$rootScope',
        '$sessionStorage',
        'RulesR',
        '$timeout',
        function($state, $scope, $rootScope, $sessionStorage, RulesR, $timeout){

            contentHeight();
            var rulesPage= this;
            var rulesTable = $rootScope.rulesTable;
            var rules = [];
            var txt, elem, ruleId, description;

            rulesPage.currentRuleData = [];
            rulesPage.editing = false;
            rulesPage.creating = false;
            rulesPage.addingPremise = false;
            rulesPage.editingPremise = -1;
            rulesPage.conclusionAttributeIsContinous = false;
            rulesPage.premiseAttributeIsContinous = false;
            rulesPage.editPremiseAttributeIsContinous = false;
            rulesPage.attributesConflict = false;
            rulesPage.rulesConflict = false;
            rulesPage.attributesConflictInPremises = false;
            rulesPage.displaingPreview = false;
            rulesPage.permission = $rootScope.userHasPermission;

            $scope.conclusionAttributesSelect = {};
            $scope.conclusionOperatorsSelect = {};
            $scope.conclusionValuesSelect = {};
            $scope.premiseAttributesSelect = {};
            $scope.premiseOperatorsSelect = {};
            $scope.premiseValuesSelect = {};
            $scope.premises = [];


            rulesPage.setOperatorString = function ( operator ){
                var txt;
                switch(operator){
                    case '==': txt = "is equal to";
                        break;
                    case '!=': txt = "is different than";
                        break;
                    case '<': txt = "is smaller than";
                        break;
                    case '>': txt = "is greater than";
                        break;
                    case '<=': txt = "is smaller or equal to";
                        break;
                    case '=>': txt = "is greater or equal to";
                        break;
                }
                return txt;
            };

            /**
             * Building rules strings
             * */

            for( var i = 0; i < rulesTable.length; i++ ){
                txt = "";
                elem = {};
                for( var j = 0; j < rulesTable[i].length; j++ ) {
                    if( typeof rulesTable[i][j] != 'undefined'){
                        //console.log(rulesTable[i][j]);
                        if (j != rulesTable[i].length - 1) {
                            if (j != 0) {
                                if (rulesTable[i][j - 1].isConclusion != 'undefined') {
                                    if (rulesTable[i][j - 1].isConclusion == rulesTable[i][j].isConclusion) {
                                        txt += "AND "
                                    } else {
                                        txt += "IF "
                                    }
                                }
                            }
                            txt += rulesTable[i][j].attribute.name + " " + rulesTable[i][j].operator + " ";
                            //rulesPage.setOperatorString( rulesTable[i][j].operator ) + " ";

                            if (rulesTable[i][j].attribute.type == 'continous') {
                                txt += rulesTable[i][j].continousValue + " ";
                            } else {
                                txt += rulesTable[i][j].value.name + " ";
                            }
                        } else {
                            ruleId = rulesTable[i][j].ruleId;
                            description = rulesTable[i][j].description;
                        }
                    }else{
                        //console.log('u:'+rulesTable[i][j]);
                        txt = 'Incorrect rule!';
                        break;
                    }
                }
                elem = {
                    id: ruleId,
                    rule: txt,
                    description: description
                };
                rules.push( elem );
            }
            $scope.items = rules;



            /**
             * Modals functions
             * */

            $('#ruleModal').on('hidden.bs.modal', function (e) {
                rulesPage.conclusionAttributeIsContinous = false;
                rulesPage.editing = false;
                rulesPage.creating = false;
                rulesPage.attributesConflict = false;
                rulesPage.rulesConflict = false;
                rulesPage.addingPremise = false;
                rulesPage.editingPremise = -1;
                rulesPage.displayPreview(false);
                rulesPage.clearConclusionForm();
                rulesPage.clearPremiseForm();
                $scope.premises = [];
            });

            $('#deletePremiseModal').on('hidden.bs.modal', function (e) {
                $("body").addClass('modal-open');
            });

            rulesPage.openModal = function( edit, create, id, index ){
                /**
                 * Odpowiednie przygotowanie modala w zale�no�ci od 'trybu'
                 * oraz jego otwarcie
                 * */
                if( create ) {
                    rulesPage.creating = true;
                    $scope.ruleModalTitle = 'Creating';
                    $scope.conclusionAttributes = $rootScope.attributesTable;
                    $scope.premiseAttributes = $rootScope.attributesTable;
                }else if( edit ){
                    rulesPage.editing = true;
                    $scope.ruleModalTitle = 'Editing';
                    $scope.conclusionAttributes = $rootScope.attributesTable;
                    $scope.premiseAttributes = $rootScope.attributesTable;
                    rulesPage.setModal( edit, id, index );
                }else{
                    $scope.ruleModalTitle = 'Rule';
                    rulesPage.setModal( edit, id, index );
                }

                $("#ruleModal").modal("show");
            };

            $scope.$on("myEvent", function (event, args) {
                $scope.conclusionAttributes = $rootScope.attributesTable;
                $scope.premiseAttributes = $rootScope.attributesTable;
                rulesPage.currentRuleData = args.ruleData;
                rulesPage.editing = true;

                var rule = args.rule;
                var conclusion = rule.shift();
                $scope.premises = rule;
                $scope.conclusionAttributesSelect.selected = conclusion.attribute;
                if( conclusion.attribute.type == 'continous' ){
                    $rootScope.conclusionAttributeIsContinous = true;
                    $scope.conclusionOperators = [{name: '=='},{name: '!='},{name: '<'},{name: '<='},{name: '>'},{name: '=>'}];
                    $scope.conclusionContinousValue = conclusion.continousValue;
                    $timeout(function(){
                        $scope.conclusionOperatorsSelect.selected = {name: conclusion.operator};
                    },100);
                }else{
                    $scope.conclusionValues = conclusion.attribute.values;
                    $scope.conclusionOperators = [{name: '=='},{name: '!='}];
                    $timeout(function(){
                        $scope.conclusionValuesSelect.selected = conclusion.value;
                        $scope.conclusionOperatorsSelect.selected = {name: conclusion.operator};
                    },100);
                }
                if( args.ruleData.description != null )
                    $scope.ruleDescription = args.ruleData.description;
            });

            rulesPage.setModal = function( edit, id, index ){
                /**
                 * Ustawienie warto�ci dla modala, gdy
                 * regu�a jest edytowana lub tylko do odczytu
                 * */
                var rule = new Array();
                for( var i = 0; i < $rootScope.rulesTable[index].length; i ++ ){
                    rule[i] = $rootScope.rulesTable[index][i];
                    if( i != 0 && i != $rootScope.rulesTable[index].length-1 )
                        rule[i].editData = {
                            attribute:{},
                            operator:{},
                            value:{}
                        };
                }
                var conclusion = rule[0];
                var ruleData = rule.pop();
                rulesPage.currentRuleData = ruleData;

                if( edit ){
                    rule.shift();
                    $scope.premises = rule;

                    $scope.conclusionAttributesSelect.selected = conclusion.attribute;
                    if( conclusion.attribute.type == 'continous' ){
                        rulesPage.conclusionAttributeIsContinous = true;
                        $scope.conclusionOperators = [{name: '=='},{name: '!='},{name: '<'},{name: '<='},{name: '>'},{name: '=>'}];
                        $scope.conclusionContinousValue = conclusion.continousValue;
                        $timeout(function(){
                            $scope.conclusionOperatorsSelect.selected = {name: conclusion.operator};
                        },100);
                    }else{
                        $scope.conclusionValues = conclusion.attribute.values;
                        $scope.conclusionOperators = [{name: '=='},{name: '!='}];
                        $timeout(function(){
                            $scope.conclusionValuesSelect.selected = conclusion.value;
                            $scope.conclusionOperatorsSelect.selected = {name: conclusion.operator};
                        },100);
                    }
                    if( ruleData.description != null )
                        $scope.ruleDescription = ruleData.description;
                }else{
                    $scope.ruleItems = rule;
                    if( ruleData.description != null ){
                        $scope.ruleReadDescription = ruleData.description;
                    }else{
                        $scope.ruleReadDescription = '';
                    }
                }

            };

            rulesPage.setConclusionValues = function( attribute ){
                /**
                 * Ustawienie listy warto�ci dla wybranego atrybutu
                 * w konkluzji oraz odpowieniej listy operator�w
                 * */
                $scope.conclusionOperatorsSelect = {};
                $scope.conclusionValuesSelect = {};
                $scope.conclusionContinousValue = '';

                if( attribute.type == 'continous'){
                    rulesPage.conclusionAttributeIsContinous = true;
                    $scope.conclusionOperators = [{name: '=='},{name: '!='},{name: '<'},{name: '<='},{name: '>'},{name: '=>'}];
                }else{
                    rulesPage.conclusionAttributeIsContinous = false;
                    $scope.conclusionOperators = [{name: '=='},{name: '!='}];
                    $scope.conclusionValues = [];
                    $scope.conclusionValues = attribute.values;
                }
            };

            rulesPage.setPremiseValues = function( attribute ){
                /**
                 * Ustawienie listy warto�ci dla wybranego atrybutu
                 * w przes�ance oraz odpowieniej listy operator�w
                 * */
                $scope.premiseOperatorsSelect = {};
                $scope.premiseValuesSelect = {};
                if( attribute.type == 'continous'){
                    rulesPage.premiseAttributeIsContinous = true;
                    $scope.premiseOperators = [{name: '=='},{name: '!='},{name: '<'},{name: '<='},{name: '>'},{name: '=>'}];
                }else{
                    rulesPage.premiseAttributeIsContinous = false;
                    $scope.premiseOperators = [{name: '=='},{name: '!='}];
                    $scope.premiseValues = [];
                    $scope.premiseValues = attribute.values;
                }
            };

            rulesPage.displayPreview = function( flag ){
                /**
                 * Ustawia warto�� zmiennej steruj�cej wy�wietlaniem
                 * podgl�du w 'trybach': 'edit' i 'create'
                 * */
                rulesPage.displaingPreview = flag;
            };

            rulesPage.setAddingPremise = function( flag ){
                /**
                 * Ustawia warto�� zmiennej steruj�cej wy�wietlaniem
                 * formularza przes�anki
                 * */
                rulesPage.addingPremise = flag;
            };

            rulesPage.clearPremiseForm = function(){
                /**
                 * Czy�ci pola formularza przes�anki
                 * */
                $scope.premiseAttributesSelect.selected = "";
                $scope.premiseOperatorsSelect.selected = "";
                $scope.premiseOperators = [];
                $scope.premiseValuesSelect.selected = "";
                $scope.premiseValues = [];
                $scope.premiseContinousValue = ""
            };

            rulesPage.clearConclusionForm = function(){
                /**
                 * Czy�ci pola formularza konkluzji i opisu regu�y
                 * */
                $scope.conclusionAttributesSelect.selected = "";
                $scope.conclusionOperatorsSelect.selected = "";
                $scope.conclusionOperators = [];
                $scope.conclusionValuesSelect.selected = "";
                $scope.conclusionValues = [];
                $scope.conclusionContinousValue = "";
                $scope.ruleDescription = "";
            };

            /**
             * Creating new rule
             * */

            rulesPage.createNewRule = function(){
                var continousValue, value;
                var premises = [];

                if( $scope.conclusionAttributesSelect.selected.type == 'continous'){
                    continousValue = $scope.conclusionContinousValue;
                    value = null;
                }else{
                    continousValue = null;
                    value = $scope.conclusionValuesSelect.selected;
                }

                var conclusion = {
                    "attribute": $scope.conclusionAttributesSelect.selected,
                    "attributeOrder": 1,
                    "continousValue": continousValue,
                    "id": null,
                    "isConclusion": true,
                    "isFact": false,//? false
                    "isGoal": false,//? false
                    "operator": $scope.conclusionOperatorsSelect.selected.name,
                    "value": value
                };

                premises.unshift(conclusion);
                premises = premises.concat($scope.premises);

                var createData = {
                    "attributeValues": premises,
                    "description": $scope.ruleDescription,
                    "id": null,
                    "knowledgeBase": null,
                    "baseId": $sessionStorage.currentBaseId
                };

                RulesR.create(createData).$promise.then(
                    function success(response){
                        elem = {
                            id: response.id,
                            rule: rewriteString(premises),//txt
                            description: response.description
                        };

                        premises.push({
                            ruleId: response.id,
                            index: rules.length,
                            description: $scope.ruleDescription,
                            verification:{
                                inconsistency: null,
                                absorption: null,
                                sameness: null
                            }
                        });

                        $rootScope.rulesTable.push(premises);//pushing to rules
                        rules.push( elem );//pushing to string
                        $("#ruleModal").modal('hide');
                    },
                    function error(response){
                        console.log(response);
                    }
                );
            };

            function rewriteString(premises){
                /**
                 * Funkcja tworzy nowego stringa dla
                 * tworzonej lub edytowanej regu�y
                 * */
                var txt = "";
                for( var i = 0; i < premises.length; i++ ){
                    if( premises[i].attribute.type == 'continous' ){
                        premises[i].value = premises[i].continousValue;
                    }

                    if( i != 0 ){
                        if( premises[i].isConclusion != 'undefined' ){
                            if( premises[i-1].isConclusion == premises[i].isConclusion){
                                txt += "AND "
                            }else{
                                txt += "IF "
                            }
                        }
                    }

                    txt += premises[i].attribute.name + " " + premises[i].operator + " ";
                        //rulesPage.setOperatorString( premises[i].operator ) + " ";

                    if( premises[i].attribute.type == 'continous' ){
                        txt += premises[i].continousValue + " ";
                    }else{
                        txt += premises[i].value.name + " ";
                    }

                    delete premises[i].id;
                    delete premises[i].isFact;
                    delete premises[i].isGoal;
                    delete premises[i].continousValue;
                }
                return txt;
            }

            /**
             * Editing rule
             * */
            rulesPage.editRule = function( edit, create, id, index, event ){
                $rootScope.animateOff( event );
                rulesPage.openModal( edit, create, id, index );
            };

            rulesPage.createDataToSave = function(){
                var continousValue, value;
                var premises = [];
                var id = rulesPage.currentRuleData.ruleId;
                var deferred = $.Deferred();

                if( $scope.conclusionAttributesSelect.selected.type == 'continous'){
                    continousValue = $scope.conclusionContinousValue;
                    value = null;
                }else{
                    continousValue = null;
                    value = $scope.conclusionValuesSelect.selected;
                }

                var conclusion = {
                    "attribute": $scope.conclusionAttributesSelect.selected,
                    "attributeOrder": 1,
                    "continousValue": continousValue,
                    "id": null,
                    "isConclusion": true,
                    "isFact": false,//?
                    "isGoal": false,//?
                    "operator": $scope.conclusionOperatorsSelect.selected.name,
                    "value": value
                };

                premises.unshift(conclusion);
                premises = premises.concat($scope.premises);


                var updateData = {
                    "attributeValues": premises,
                    "description": $scope.ruleDescription,
                    "id": id,
                    "knowledgeBase": null,
                    "baseId": $sessionStorage.currentBaseId
                };

                var obj = {
                    updateData: {},
                    premises: [],
                    elem: {
                        id: id,
                        rule: rewriteString(premises),
                        description: $scope.ruleDescription
                    }
                };
                rewritePremises(premises).done(function(result){
                    premises = result;

                    updateData.attributeValues = premises;
                    obj.updateData = updateData;
                    obj.premises = premises;
                    deferred.resolve(obj);
                });

                return deferred;
            };

            function rewritePremises(premises){
                var deferred = $.Deferred();
                for( var i = 0 ; i < premises.length; i++ ){
                    if( premises[i].attribute.type == 'continous' && premises[i].value != null ){
                        premises[i].continousValue = premises[i].value;
                        premises[i].value = null;
                    }

                    if( i == premises.length - 1 ) deferred.resolve(premises);
                }
                return deferred;
            }

            rulesPage.saveChanges = function(){

                rulesPage.createDataToSave().promise().done(function(obj){

                    RulesR.update(obj.updateData).$promise.then(
                        function success(response){
                            var index = rulesPage.currentRuleData.index;

                            obj.premises.push({
                                ruleId: response.id,
                                index: index,
                                description: $scope.ruleDescription,
                                verification:{
                                    inconsistency: null,
                                    absorption: null,
                                    sameness: null
                                }
                            });

                            $rootScope.rulesTable[index] = obj.premises;
                            rules[index] = obj.elem;
                            $("#ruleModal").modal('hide');
                        }, function error(response){
                            console.log(response);
                        }
                    );
                });
            };

            /**
             * Deleting rule
             * */

            rulesPage.deleteInfo = function( id, index, event ){
                $rootScope.animateOff( event );
                $scope.deleteRuleId = id;
                $scope.deleteRuleIndex = index;
                $scope.deleteRuleName = index + 1;
                $("#deleteModal").modal("show");
            };

            rulesPage.deleteRule = function( id, index ){
                RulesR.delete({baseId: $sessionStorage.currentBaseId, ruleId: id}).$promise.then(
                    function success(response){
                        $rootScope.rulesTable.splice(index, 1);
                        rules.splice(index, 1);
                    }, function error(response){
                        console.log(response);
                    }
                );
            };

            /**
             * Adding new premise
             * */

            rulesPage.addPremise = function(){
                var continousValue, value;

                if( $scope.premiseAttributesSelect.selected.type == 'continous'){
                    continousValue = $scope.premiseContinousValue;
                    value = null;
                }else{
                    continousValue = null;
                    value = $scope.premiseValuesSelect.selected;
                }

                var createPremise = {
                    "attribute": $scope.premiseAttributesSelect.selected,
                    "attributeOrder": $scope.premises.length + 2,
                    "continousValue": continousValue,
                    "id": null,
                    "isConclusion": false,
                    "isFact": false,//?
                    "isGoal": false,//?
                    "operator": $scope.premiseOperatorsSelect.selected.name,
                    "value": value,
                    "editData": {
                        attribute:{},
                        operator:{},
                        value: {}
                    }
                };
                console.log(createPremise);
                $scope.premises.push(createPremise);
                rulesPage.setAddingPremise(false);
                rulesPage.clearPremiseForm();
            };

            /**
             * Editing premise
             * */

            rulesPage.editPremise = function( index, event ){
                /**
                 * Pokazuje formularz do edycji przes�anki
                 * oraz uzupe�nia go odpowiednimi warto�ciami
                 * */
                $rootScope.animateOff(event);
                var premise = $scope.premises[index];
                $scope.premises[index].editData.attribute.selected = premise.attribute;

                if(premise.attribute.type == 'continous'){
                    rulesPage.editPremiseAttributeIsContinous = true;
                    $scope.premises[index].editData.value.continousValue = premise.continousValue;
                    $scope.premises[index].editData.operator.array = [];
                    $scope.premises[index].editData.operator.array = [{name: '=='},{name: '!='},{name: '<'},{name: '<='},{name: '>'},{name: '=>'}];
                    $timeout(function(){
                        $scope.premises[index].editData.operator.selected = {name: premise.operator};
                        rulesPage.editingPremise = index;
                    }, 100);
                }else{
                    rulesPage.editPremiseAttributeIsContinous = false;
                    $scope.premises[index].editData.value.array = [];
                    $scope.premises[index].editData.value.array = premise.attribute.values;
                    $scope.premises[index].editData.operator.array = [];
                    $scope.premises[index].editData.operator.array = [{name: '=='},{name: '!='}];
                    $timeout(function(){
                        $scope.premises[index].editData.value.selected = premise.value;
                        $scope.premises[index].editData.operator.selected = {name: premise.operator};
                        rulesPage.editingPremise = index;
                    }, 200);
                }
            };

            rulesPage.savePremiseChanges = function( index ){
                var continousValue, value;
                var premise = $scope.premises[index];

                if( premise.editData.attribute.selected.type == 'continous'){
                    continousValue = premise.editData.value.continousValue;
                    value = null;
                }else{
                    continousValue = null;
                    value = premise.editData.value.selected;
                }

                var updatePremise = {
                    "attribute": premise.editData.attribute.selected,
                    "attributeOrder": index + 2,
                    "continousValue": continousValue,
                    "id": null,
                    "isConclusion": false,
                    "isFact": false,//?
                    "isGoal": false,//?
                    "operator": premise.editData.operator.selected.name,
                    "value": value,
                    "editData": {
                        attribute:{},
                        operator:{},
                        value: {}
                    }
                };

                //$scope.premises[index] = {};
                //$scope.premises[index] = updatePremise;
                $scope.premises.splice(index,1, updatePremise);
                rulesPage.editingPremise = -1;
                rulesPage.editPremiseAttributeIsContinous = false;
            };

            rulesPage.cancelPremiseChanges = function(){
                /**
                 * Odpowiednie ustawienie zmiennych steruj�cych
                 * przy rezygnacji z edycji przes�anki
                 * */
                rulesPage.editingPremise = -1;
                rulesPage.editPremiseAttributeIsContinous = false;
                rulesPage.validateAttribute();
            };

            rulesPage.setPremiseEditValues = function( attribute, index ){
                /**
                 * Ustawienie odpowiednich operator�w i warto�ci
                 * przy zmianie atrybutu
                 * */
                $scope.premises[index].editData.operator.selected = "";
                $scope.premises[index].editData.value.selected = "";
                $scope.premises[index].editData.value.continousValue = "";
                if( attribute.type == 'continous' ){
                    rulesPage.editPremiseAttributeIsContinous = true;
                    $scope.premises[index].editData.operator.array = [];
                    $scope.premises[index].editData.operator.array = [{name: '=='},{name: '!='},{name: '<'},{name: '<='},{name: '>'},{name: '=>'}];
                }else{
                    rulesPage.editPremiseAttributeIsContinous = false;
                    $scope.premises[index].editData.operator.array = [];
                    $scope.premises[index].editData.operator.array = [{name: '=='},{name: '!='}];
                    $scope.premises[index].editData.value.array = attribute.values;
                }

            };

            /**
             * Deleting premise
             * */

            rulesPage.deletePremiseInfo = function( index, event ){
                $rootScope.animateOff(event);
                $scope.deletePremiseNumber = index + 1;
                $scope.deletePremiseIndex = index;
                $("#deletePremiseModal").modal('show');
            };

            rulesPage.deletePremise = function( index ){
                $scope.premises.splice(index, 1);
                rulesPage.validateAttribute();
            };

            /**
             * Validating attributes and rule
             * */

            rulesPage.validateAttribute = function(editIndex){
                /**
                 * Funkcja sprawdzaj�ca, czy w regule nie zosta�
                 * wykorzystany ten sam atrybut wi�cej ni� 1 raz
                 * */
                var obj, conclusionAttribute = '', premiseAttribute = '', editPremiseAttribute = '', premises = $scope.premises, index;

                if (typeof $scope.conclusionAttributesSelect.selected != 'undefined')
                    conclusionAttribute = $scope.conclusionAttributesSelect.selected.name;

                if (typeof $scope.premiseAttributesSelect.selected != 'undefined')
                    premiseAttribute = $scope.premiseAttributesSelect.selected.name;

                if( typeof arguments[0] != 'undefined' && rulesPage.editingPremise == arguments[0] ){
                    if (typeof $scope.premises[arguments[0]].editData.attribute.selected != 'undefined'){
                        editPremiseAttribute = $scope.premises[arguments[0]].editData.attribute.selected.name;
                        index = editIndex;//arguments[0];
                    }
                }else{
                   index = -1;
                }

                obj = checkAttributeConflict(conclusionAttribute, premiseAttribute, premises, index, editPremiseAttribute );

                rulesPage.attributesConflict = obj.conflict;
                rulesPage.attributesConflictInPremises = obj.editPremiseConflict;
                $scope.conflictAttributeName = obj.txt;
                return obj.conflict;
            };

            rulesPage.validateRule = function(){
                /**
                 * Funkcja sprawdzaj�ca, czy taka regu�a
                 * ju� nie istnieje w bazie wiedzy
                 * */
                var rules =  $rootScope.rulesTable;
                var premises = $scope.premises;
                var samePremises = 0, notValid = false, value, continousValue;

                if( $scope.conclusionAttributesSelect.selected.type == 'continous' ){
                    value = null;
                    continousValue = $scope.conclusionContinousValue;
                }else{
                    value = $scope.conclusionValuesSelect.selected;
                    continousValue = null;
                }

                var conclusion = {
                    attribute: $scope.conclusionAttributesSelect.selected,
                    operator: $scope.conclusionOperatorsSelect.selected.name,
                    continousValue: continousValue,
                    value: value
                };

                var rule = [];
                rule.push(conclusion);

                for( var l = 0; l < premises.length; l++ ){
                    rule.push(premises[l]);
                }
                rule.push({validation:""});

                //przegl�d wszystkich regu�
                for( var i = 0; i < rules.length; i++ ){
                    samePremises = 0;

                    samePremises = samenessComparison(rule, rules[i]);

                    if( rulesPage.editing ){
                        if( samePremises == premises.length && i != rulesPage.currentRuleData.index){
                            notValid = true;
                            break;
                        }
                    }else{
                        if( samePremises == premises.length ){
                            notValid = true;
                            break;
                        }
                    }

                }//for

                rulesPage.rulesConflict = notValid;
                return notValid;
            };

            /**
             * Other
             * */

            rulesPage.animate = function( event ){
                $rootScope.animate( event );
            };





            rulesPage.toBase = function(){
                $state.go('base');
            };

        }
    ]);