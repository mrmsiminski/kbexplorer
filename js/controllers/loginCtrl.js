/**
 * Created by M. on 11.07.2016.
 * Last edit: 12.09.2016 15:25
 */

angular
    .module('kbExplorer')
    .controller('loginCtrl', [
        '$state',
        function($state){

            var loginPage = this;

            loginPage.loginUser = function(){
                var url = _APP.urls.requestUrl + _APP.auth.redirectUrl;
                location.assign(url);
            };

            loginPage.registerUser = function(){
                //$state.go('registration');
                var url = 'http://les.szymon.net.pl/#/register';
                location.assign(url);
            };

        }
    ]);