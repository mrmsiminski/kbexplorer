/**
 * Created by M. on 07.04.2016.
 * Last edit: 06.07.2016 23:55
 */

angular
    .module('kbExplorer')
    .controller('factsCtrl', [
        '$state',
        '$scope',
        '$rootScope',
        '$sessionStorage',
        'FactsR',
        '$timeout',
        function($state, $scope, $rootScope, $sessionStorage, FactsR, $timeout){

            contentHeight();
            var factPage = this;
            factPage.attributeIsContinous = false;
            factPage.editing = false;
            factPage.creating = false;
            factPage.currentFact = {};
            factPage.factConflict = false;
            factPage.permission = $rootScope.userHasPermission;

            factPage.attibute = {};
            factPage.operator = '';
            factPage.value = [];

            $scope.attributesSelect = {};
            $scope.operatorsSelect = {};
            $scope.valuesSelect = {};
            $scope.items = $rootScope.factsTable;

            /**
             * Modal functions
             * */

            factPage.openModal = function( edit, create, id ){
                /**
                 * Odpowiednie przygotowanie modala w zale�no�ci od 'trybu'
                 * oraz jego otwarcie
                 * */
                if( create ){
                    factPage.creating = true;
                    $scope.factModalTitle = 'Creating';
                    $scope.attributes = $rootScope.attributesTable;
                }else if( edit ){
                    factPage.editing = true;
                    $scope.factModalTitle = 'Editing';
                    $scope.attributes = $rootScope.attributesTable;
                    factPage.setModal( edit, id );
                }else{
                    $scope.factModalTitle = 'Fact';
                    factPage.setModal( edit, id );
                }

                $("#factModal").modal("show");
            };

            factPage.setModal = function( edit, id ){
                /**
                 * Ustawienie warto�ci dla modala, gdy
                 * fakt jest edytowany lub tylko do odczytu
                 * */
                factPage.currentFact = $rootScope.hashMapF[id];
                if( edit ){
                    if( factPage.currentFact.attribute.type == 'continous' ){
                        factPage.attributeIsContinous = true;
                        $scope.operators = [{name: '=='},{name: '!='},{name: '<'},{name: '<='},{name: '>'},{name: '=>'}];
                        $scope.continousValue = factPage.currentFact.continousValue;
                        $timeout(function(){
                            $scope.attributesSelect.selected = factPage.currentFact.attribute;
                            $scope.operatorsSelect.selected = { name: factPage.currentFact.operator };
                        },100);
                    }else{
                        $scope.operators = [{name: '=='},{name: '!='}];
                        $scope.values = factPage.currentFact.attribute.values;
                        $timeout(function(){
                            $scope.attributesSelect.selected = factPage.currentFact.attribute;
                            $scope.operatorsSelect.selected = { name: factPage.currentFact.operator };
                            $scope.valuesSelect.selected = factPage.currentFact.value;
                        },100);
                    }
                }else{
                    /*if( factPage.currentFact.attribute.query == null ){
                        $scope.attributePreview = factPage.currentFact.attribute.name;
                    }else{
                        $scope.attributePreview = factPage.currentFact.attribute.query;
                    }*/
                    $scope.attributePreview = factPage.currentFact.attribute.name;
                    $scope.operatorPreview = factPage.setOperatorString(factPage.currentFact.operator);

                    if( factPage.currentFact.attribute.type == 'continous' ){
                        $scope.valuePreview = factPage.currentFact.continousValue;
                    }else{
                        $scope.valuePreview = factPage.currentFact.value.name;
                    }
                }
            };

            factPage.clearForm = function(){
                /**
                 * Czyszczenie formularza i zmiennych steruj�cych
                 * */
                factPage.editing = false;
                factPage.creating = false;
                factPage.attributeIsContinous = false;
                factPage.factConflict = false;

                $scope.attributesSelect.selected = "";
                $scope.attributesPreview = "";
                $scope.attributes = [];
                $scope.operatorsSelect.selected = "";
                $scope.operatorsPreview = "";
                $scope.operatorsList = [];
                $scope.valuesSelect.selected = "";
                $scope.valuesPreview = "";
                $scope.values = [];
                $scope.continousValue = "";
            };

            factPage.setValues = function( attribute ){
                /**
                 * Ustawienie listy warto�ci dla wybranego atrybutu
                 * w fakcie oraz odpowieniej listy operator�w
                 * */
                $scope.operatorsSelect = {};
                $scope.valuesSelect = {};

                if( attribute.type != "continous" ) {
                    factPage.attributeIsContinous = false;
                    $scope.operators = [{name: '=='},{name: '!='}];
                    $scope.values = [];
                    $scope.values = attribute.values;
                }else{
                    factPage.attributeIsContinous = true;
                    $scope.operators = [{name: '=='},{name: '!='},{name: '<'},{name: '<='},{name: '>'},{name: '=>'}];
                }
            };

            factPage.setOperatorString = function( operator ){
                /**
                 * Przek�ada znak operatora na j�zyk naturalny
                 * */
                var txt;
                switch(operator){
                    case '==': txt = "is equal to";
                        break;
                    case '!=': txt = "is different than";
                        break;
                    case '<': txt = "is smaller than";
                        break;
                    case '>': txt = "is greater than";
                        break;
                    case '<=': txt = "is smaller or equal to";
                        break;
                    case '=>': txt = "is greater or equal to";
                        break;
                }
                return txt;
            };

            $('#factModal').on('hidden.bs.modal', function (e) {
                factPage.clearForm();
            });

            /**
             * Creating facts
             * */

            factPage.createNewFact = function(){

                var value;
                var continousValue;

                if( factPage.attributeIsContinous ){
                    value = null;
                    continousValue = $scope.continousValue;
                }else{
                    continousValue = null;
                    value = $scope.valuesSelect.selected;
                }

                var createData = {
                    "attribute": $scope.attributesSelect.selected,
                    "continousValue": continousValue,
                    "id": null,
                    "knowledgeBase": null,
                    "operator": $scope.operatorsSelect.selected.name,
                    "value": value,
                    "baseId": $sessionStorage.currentBaseId
                };

                FactsR.create( createData ).$promise.then(function success(response){
                    delete createData.baseId;
                    delete createData.knowledgeBase;

                    createData.id = response.id;
                    createData.tabPosition = $rootScope.factsTable.length;
                    $rootScope.factsTable.push( createData );
                    var key = response.id;
                    var value = $rootScope.factsTable[createData.tabPosition];
                    $rootScope.hashMapF[ key ] = value;

                    $("#factModal").modal('hide');

                }, function error(response){
                    console.log(response);
                });
            };

            /**
             * Editing facts
             * */

            factPage.editFact = function( id, event ){
                $rootScope.animateOff( event );
                factPage.openModal( true, false, id );
            };

            factPage.saveChanges = function(){
                var continousValue;
                var value;

                if( factPage.attributeIsContinous == true ){
                    continousValue = $scope.continousValue;
                    value = null;
                }else{
                    continousValue = null;
                    value = $scope.valuesSelect.selected;
                }

                var updateData = {
                    "attribute": $scope.attributesSelect.selected,
                    "continousValue": continousValue,
                    "id": factPage.currentFact.id,
                    "knowledgeBase": factPage.currentFact.knowledgeBase,
                    "operator": $scope.operatorsSelect.selected.name,
                    "value": value,
                    "baseId": $sessionStorage.currentBaseId
                };

                FactsR.update( updateData ).$promise.then(function success(response){
                    delete updateData.baseId;
                    delete updateData.knowledgeBase;

                    $rootScope.factsTable[factPage.currentFact.tabPosition] = updateData;
                    $rootScope.hashMapF[factPage.currentFact.id] = $rootScope.factsTable[factPage.currentFact.tabPosition];
                    factPage.attributeIsContinous = false;

                    $("#factModal").modal('hide');
                }, function error(response){
                    console.log(response);
                });
            };

            /**
             * Deleting facts
             * */

            factPage.deleteInfo = function( id, event ){
                $rootScope.animateOff(event);
                $scope.deleteFactId = id;
                $("#deleteModal").modal("show");
            };

            factPage.deleteFact = function( id ){
                FactsR.delete( {baseId: $sessionStorage.currentBaseId, factId: id} ).$promise.then( function success(response){
                    var index = $rootScope.factsTable.indexOf( $rootScope.hashMapF[id] );
                    $rootScope.factsTable.splice(index, 1);
                    delete $rootScope.hashMapF[id];
                }, function error(response){
                    console.log(response);
                });
            };

            /**
             * Validating facts
             * */

            factPage.validateFact = function(){
                /**
                 * Funkcja sprawdzaj�ca, czy dany fakt
                 * ju� nie istnieje
                 * */
                if( typeof  $scope.attributesSelect != 'undefined' &&
                    typeof  $scope.attributesSelect.selected != 'undefined'){

                    var exist = false;
                    var facts = $rootScope.factsTable;
                    var attribute, operator, value;

                    attribute = $scope.attributesSelect.selected;

                    if( attribute.type != 'continous' ){
                        if( typeof $scope.operatorsSelect != 'undefined' &&
                            typeof $scope.valuesSelect != 'undefined' &&
                            typeof $scope.operatorsSelect.selected !== 'undefined' &&
                            typeof $scope.valuesSelect.selected !== 'undefined'
                        ) {

                            operator = $scope.operatorsSelect.selected.name;
                            value = $scope.valuesSelect.selected;

                            for (var i = 0; i < facts.length; i++) {
                                if (facts[i].attribute.id == attribute.id) {
                                    if (facts[i].value.id == value.id) {
                                        if (facts[i].operator == operator) {
                                            exist = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            factPage.factConflict = exist;
                            return exist;
                        }
                    }else{
                        if( typeof $scope.operatorsSelect != 'undefined' &&
                            typeof $scope.operatorsSelect.selected !== 'undefined' &&
                            $scope.continousValue != ''
                        ) {
                            operator = $scope.operatorsSelect.selected.name;
                            value = $scope.continousValue;

                            for (var i = 0; i < facts.length; i++) {
                                if (facts[i].attribute.id == attribute.id) {
                                    if (facts[i].continousValue == value) {
                                        if (facts[i].operator == operator) {
                                            exist = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            factPage.factConflict = exist;
                            return exist;
                        }
                    }

                }
            };

            /**
             * Others functions
             * */

            factPage.animate = function( event ){
                $rootScope.animate(event);
            };

        }
    ]);