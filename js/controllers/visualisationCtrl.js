/**
 * Created by M. on 03.04.2016.
 * * Last edit: 08.04.2016 14:20
 */
angular
    .module('kbExplorer')
    .controller('visualisationCtrl', [
        '$scope',
        '$sessionStorage',
        '$state',
        function($scope, $sessionStorage, $state){

            contentHeight();

            $scope.info = 'Wizualizacja bazy id: ' + $sessionStorage.currentBaseId;
            var visualPage = this;


            visualPage.toBase = function(){
                $state.go('base');
            };
        }
    ]);