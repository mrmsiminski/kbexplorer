/**
 * Created by M. on 10.04.2016.
 * * Last edit: 12.09.2016 16:30
 */

angular
    .module('kbExplorer')
    .controller('headerCtrl', [
        '$scope',
        '$rootScope',
        '$http',
        '$location',
        '$sessionStorage',
        '$window',
        '$state',
        'UsersR',
        function($scope, $rootScope, $http, $location, $sessionStorage, $window, $state, UsersR) {

            var header =this;

            header.editUser = function(){

                UsersR.getOne({login: $sessionStorage.loggedUser.login}).$promise.then(
                    function success(response){
                        $rootScope.userInfo = response;
                        $rootScope.userLogin = response.login;
                        $rootScope.userMail = response.email;
                        $rootScope.userName = response.firstName;
                        $rootScope.userSurname = response.lastName;
                        $("#userModal").modal('show');
                    }, function error(response){
                        console.log(response);
                    }
                );
            };

            header.isLogin = function(){
                if( $state.current.name == 'login'|| $state.current.name == 'registration' ){
                    return false;
                }else{
                    return true;
                }
            };

            header.isState = function( state ){
                if( state == 'home' ){
                    if( $state.current.name != state && $state.current.name != 'login' ){
                        return true;
                    }
                }else if( state == 'visualisation'){
                    if( $state.current.name == state ){
                        return true;
                    }
                }else if( state == 'attributes' ){
                    if( $state.current.name == state ){
                        return true;
                    }
                }else if( state == 'facts' ){
                    if( $state.current.name == state ){
                        return true;
                    }
                }else if( state == 'rules' ){
                    if( $state.current.name == state ){
                        return true;
                    }
                }else if( state == 'verification' ){
                    if( $state.current.name == state ){
                        return true;
                    }
                }else if( state == 'inference' ){
                    if( $state.current.name == state ){
                        return true;
                    }
                }else if( state == 'registration' ){
                    if( $state.current.name == state ){
                        return true;
                    }
                }
                return false;
            };

            header.logout = function(){
                $rootScope.logoutUser();
            };

        }
    ]);
