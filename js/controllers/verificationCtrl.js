/**
 * Created by M. on 12.07.2016.
 * * Last edit: 04.08.2016 13:55
 */
angular
    .module('kbExplorer')
    .controller('verificationCtrl', [
        '$scope',
        '$sessionStorage',
        '$state',
        '$rootScope',
        '$timeout',
        'RulesR',
        function($scope, $sessionStorage, $state, $rootScope, $timeout, RulesR){

            contentHeight();
            maskHeight("#verificationMask");
            $scope.verificationBaseName = $rootScope.hashMapB[$sessionStorage.currentBaseId].name;

            var verificationPage = this;
            verificationPage.displayMode = 0;
            verificationPage.permission = $rootScope.userHasPermission;
            verificationPage.needVerifying = false;
            verificationPage.showMask = true;
            verificationPage.currentRuleData = {};

            /**
             * Pomiar czasu
             * */
            //ca�o�ciowy czas
            var allTimeStart, allTimeEnd, allTimeReal;
            //sprzczne
            var iTimeStart, iTimeEnd, iTimeReal;
            //identyczne
            var sTimeStart, sTimeEnd, sTimeReal;
            //absorbuj�ce
            var aTimeStart, aTimeEnd, aTimeReal;
            //dublowane atrybuty
            var dAttrStart, dAttrEnd, dAttrReal;


            makeVerification(function(){
                verificationPage.showMask = false;

                $timeout(function(){
                    allTimeReal = allTimeEnd - allTimeStart;
                    //iTimeReal = iTimeEnd - iTimeStart;
                    //sTimeReal = sTimeEnd - sTimeStart;
                   //aTimeReal = aTimeEnd - aTimeStart;
                    //dAttrReal = dAttrEnd - dAttrStart;

                    //console.log('Sprzeczne: ' + iTimeReal + 'ms');
                    //console.log('Identyczne: ' + sTimeReal + 'ms');
                    //console.log('Absorbuj�ce: ' + aTimeReal + 'ms');
                    //console.log('Zdublowane attr: ' + dAttrReal + 'ms');
                    console.log('Calosciowy czas ' + allTimeReal + 'ms');
                }, 1000);
            });

            /**
             * Modal function for showing: inconsistent, same or absorb rules
             * */

            verificationPage.openModal = function(indexes, mode){
                verificationPage.setModal( indexes, mode);
                $("#verificationModal").modal('show');
            };

            verificationPage.setModal = function( indexes, mode ){
                switch (mode){
                    case 1: $scope.verificationModalTitle = 'Inconsistency';
                        break;
                    case 2: $scope.verificationModalTitle = 'Sameness';
                        break;
                    case 3: $scope.verificationModalTitle = 'Absorption';
                        break;
                }

                $scope.rules = [];
                for( var i = 0; i < indexes.length; i++ ){
                    $scope.rules.push({
                        txt: createRulesString($rootScope.rulesTable[indexes[i]]),
                        ruleIndex: indexes[i]
                    });
                }
            };

            verificationPage.animate = function(event){
                $rootScope.animate(event);
            };

            $('#verificationModal').on('hidden.bs.modal', function (e) {
                if(verificationPage.needVerifying){
                    verificationPage.showMask = true;
                    $timeout(function(){
                        makeVerification(function(){
                            verificationPage.showMask = false;
                        });
                    },100);
                }
            });

            $('#ruleModal').on('hidden.bs.modal', function (e) {
                $("body").addClass('modal-open');
            });

            $('#deleteModal').on('hidden.bs.modal', function (e) {
                $("body").addClass('modal-open');
            });

            /**
             * Editing incorrect rules
             * */

            verificationPage.editRule = function(index, event, modalIndex){
                $rootScope.animateOff(event);
                verificationPage.modalIndex = modalIndex;
                setRuleModal(index, function(){
                    $("#ruleModal").modal('show');
                });
            };

            verificationPage.saveChanges = function(data){
                verificationPage.needVerifying = true;

                RulesR.update(data.updateData).$promise.then(
                    function success(response){
                        var index = verificationPage.currentRuleData.index;

                        data.premises.push({
                            ruleId: response.id,
                            index: index,
                            description: $scope.ruleDescription,
                            verification:{
                                inconsistency: null,
                                absorption: null,
                                sameness: null
                            }
                        });

                        $rootScope.rulesTable[index] = data.premises;
                        $scope.rules[verificationPage.modalIndex] = {
                            txt: createRulesString(data.premises),
                            ruleIndex: index
                        };
                        $("#ruleModal").modal('hide');
                    }, function error(response){
                        console.log(response);
                    }
                );
            };

            function setRuleModal(index, callback){
                var rule = new Array();
                for( var i = 0; i < $rootScope.rulesTable[index].length; i ++ ){
                    rule[i] = $rootScope.rulesTable[index][i];
                    if( i != 0 && i != $rootScope.rulesTable[index].length-1 )
                        rule[i].editData = {
                            attribute:{},
                            operator:{},
                            value:{}
                        };
                }

                var ruleData = rule.pop();
                verificationPage.currentRuleData = ruleData;

                $scope.$broadcast("myEvent", {rule: rule, ruleData: ruleData});
                callback();
            }

            /**
             * Deleting incorrect rules
             * */

            verificationPage.deleteInfo = function(index, event, modalIndex){
                $rootScope.animateOff(event);
                $scope.deleteRuleName = index + 1;
                $scope.deleteRuleIndex = index;
                $scope.ruleModalIndex = modalIndex;
                $("#deleteModal").modal('show');
            };

            verificationPage.deleteRule = function(index, modalIndex){
                var id = $rootScope.rulesTable[index][$rootScope.rulesTable[index].length - 1].ruleId;
                verificationPage.needVerifying = true;

                RulesR.delete({baseId: $sessionStorage.currentBaseId, ruleId: id}).$promise.then(
                    function success(response){
                        $rootScope.rulesTable.splice(index, 1);
                        $scope.rules.splice(modalIndex, 1);
                    }, function error(response){
                        console.log(response);
                    }
                );
            };

            /**
             * Verifying functions
             * */

            function makeVerification(callback){
                allTimeStart = performance.now();
                verificationPage.displayMode = 0;
                verificationPage.needVerifying = false;

                $timeout(function(){

                    var rules = $rootScope.rulesTable;
                    var rule1, rule2, doubledAttributesRules = [];

                    //przegl�d wszystkich regu�
                    for( var i = 0; i < rules.length; i++ ) {
                        rule1 = rules[i];
                        //przegl�d wszystkich nast�puj�cych regu�
                        for (var j = i + 1; j < rules.length; j++) {
                            rule2 = rules[j];
                            inconsistencyVerification( rule1, rule2, rules, i, j );
                            samenessVerification( rule1, rule2, rules, i, j );
                            absorptionVerification( rule1, rule2, rules, i, j );
                        }
                        doubledAttributesVerification( rule1, doubledAttributesRules, i );

                        if( i == rules.length - 1 ) allTimeEnd = performance.now();
                    }

                    $scope.items = createRapport( rules, 2 );
                    $scope.items2 = createRapport( rules, 1 );
                    $scope.items3 = createRapport( rules, 3 );
                    $scope.itmes4 = doubledAttributesRules;
                    $scope.doubledNumber = doubledAttributesRules.length;
                    callback();
                },200);
                //allTimeEnd = performance.now();
            }

            function createRapport( rules, mode ){
                var inconsistencyIndexes, inconsistentRules = [], counter = 0, inconsistencyNumbers,
                    number, i, j, rulesNumber = 0;

                switch( mode ){
                    case 1: //sameness
                        for( i = 0; i < rules.length; i++ ){
                            inconsistencyIndexes = rules[i][rules[i].length - 1].verification.sameness;
                            inconsistencyNumbers = [];
                            if( inconsistencyIndexes != null ){
                                for( j = 0; j< inconsistencyIndexes.length; j++ ){
                                    if( inconsistencyIndexes[j] != i ){
                                        rules[inconsistencyIndexes[j]][rules[inconsistencyIndexes[j]].length - 1].verification.sameness = null;
                                    }
                                    number = parseInt(inconsistencyIndexes[j]) + 1;
                                    inconsistencyNumbers.push(number);
                                }
                                inconsistentRules[counter] = {
                                    indexes: inconsistencyIndexes,
                                    numbers: inconsistencyNumbers
                                };
                                counter++;
                                rulesNumber += inconsistencyIndexes.length;
                            }
                            rules[i][rules[i].length - 1].verification.sameness = null;
                        }
                        $scope.samenessNumber = rulesNumber;
                        break;
                    case 2: //inconsistency
                        for( i = 0; i < rules.length; i++ ){
                            inconsistencyIndexes = rules[i][rules[i].length - 1].verification.inconsistency;
                            inconsistencyNumbers = [];
                            if( inconsistencyIndexes != null ){
                                for( j = 0; j< inconsistencyIndexes.length; j++ ){
                                    if( inconsistencyIndexes[j] != i ){
                                        rules[inconsistencyIndexes[j]][rules[inconsistencyIndexes[j]].length - 1].verification.inconsistency = null;
                                    }
                                    number = parseInt(inconsistencyIndexes[j]) + 1;
                                    inconsistencyNumbers.push(number);
                                }
                                inconsistentRules[counter] = {
                                    indexes: inconsistencyIndexes,
                                    numbers: inconsistencyNumbers
                                };
                                counter++;
                                rulesNumber += inconsistencyIndexes.length;
                            }
                            rules[i][rules[i].length - 1].verification.inconsistency = null;
                        }
                        $scope.inconsistencyNumber = rulesNumber;
                        break;
                    case 3: //absorption
                        var rule1, rule2;

                        for( i = 0; i < rules.length; i++ ){
                            inconsistencyIndexes = rules[i][rules[i].length - 1].verification.absorption;
                            inconsistencyNumbers = [];
                            if( inconsistencyIndexes != null ){
                                rule1 = rules[i][rules[i].length - 1];
                                for( j = 0; j < inconsistencyIndexes.length; j++ ) {
                                    if (inconsistencyIndexes[j] != i) {
                                        rule2 = rules[inconsistencyIndexes[j]][rules[inconsistencyIndexes[j]].length - 1];
                                        if (rule2.verification.absorption != null &&
                                            rule2.verification.absorption.length < rule1.verification.absorption.length) {

                                            rule2.verification.absorption = null;
                                        } else if (rule2.verification.absorption != null &&
                                            rule2.verification.absorption.length > rule1.verification.absorption.length) {
                                            rule1.verification.absorption = null;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        for( i = 0; i < rules.length; i++ ){
                            if( rules[i][rules[i].length - 1].verification.absorption != null ){
                                inconsistencyIndexes = rules[i][rules[i].length - 1].verification.absorption;
                                for( j = 0; j < inconsistencyIndexes.length; j++ ){
                                    number = parseInt(inconsistencyIndexes[j]) + 1;
                                    inconsistencyNumbers.push(number);
                                }
                                inconsistentRules[counter] = {
                                    indexes: inconsistencyIndexes,
                                    numbers: inconsistencyNumbers
                                };
                                counter++;
                                rulesNumber += inconsistencyIndexes.length;
                            }
                            $rootScope.rulesTable[i][$rootScope.rulesTable[i].length - 1].verification.absorption = null;
                        }
                        $scope.absorptionNumber = rulesNumber;
                        break;
                }

                return inconsistentRules;
            }

            function inconsistencyVerification( rule1, rule2, rules, i, j ){

                var samePremises = 0;
                //por�wnanie obu regu�
                samePremises = inconsistencyComparison(rule1, rule2);

                //sprawdzenie, czy regu�y s� sprzeczne
                if( samePremises == rule1.length - 2 ){
                   if( rules[i][rules[i].length - 1].verification.inconsistency == null ){
                       rules[i][rules[i].length - 1].verification.inconsistency = [];
                       rules[i][rules[i].length - 1].verification.inconsistency.push(i);
                       rules[i][rules[i].length - 1].verification.inconsistency.push(j);
                   }else{
                       rules[i][rules[i].length - 1].verification.inconsistency.push(j);
                   }
                }

            }

            function inconsistencyComparison( rule1, rule2){
                var samePremises = 0;

                //sprawdzenie d�ugo�ci obu regu�
                if( rule1.length == rule2.length ){
                    //sprawdzenie atrybut�w konkluzji
                    if( rule1[0].attribute.name == rule2[0].attribute.name ){
                        //sprawdzenie warto�ci konkluzji
                        if( ( rule1[0].attribute.type == 'continous' && rule1[0].continousValue == rule2[0].continousValue )||
                            ( rule1[0].attribute.type != 'continous' && rule1[0].value.name == rule2[0].value.name )
                        ){
                            //sprawdzenie operator�w konkluzji
                            if( rule1[0].operator != rule2[0].operator ){
                                //przegl�d wszystkich przes�anek z 1 regu�y
                                for( var k = 1; k < rule1.length - 1; k ++ ){
                                    if( samePremises != k - 1 ) break;
                                    //przegl�d wszystkich przes�anek z 2 regu�y
                                    for( var l = 1; l < rule2.length - 1; l++ ){
                                        //por�wnanie atrybut�w przes�anek
                                        if( rule1[k].attribute.name == rule2[l].attribute.name ){
                                            //por�wnanie warto�ci przes�anek
                                            if( ( rule1[k].attribute.type == 'continous' && rule1[k].continousValue == rule2[l].continousValue )||
                                                ( rule1[k].attribute.type != 'continous' && rule1[k].value.name == rule2[l].value.name )
                                            ){
                                                //por�wnanie operator�w przes�anek
                                                if( rule1[k].operator == rule2[l].operator ){
                                                    samePremises++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return samePremises;
            }

            function absorptionVerification(rule1, rule2, rules, i, j){

                var samePremises = 0;
                //por�wnanie obu regu� 1
                samePremises = absorptionComparison(rule1, rule2);

                //sprawdzenie, czy regu�y s� poch�aniaj�ce
                if( samePremises == rule1.length - 2 ){
                    if( rules[i][rules[i].length - 1].verification.absorption == null ){
                        rules[i][rules[i].length - 1].verification.absorption = [];
                        rules[i][rules[i].length - 1].verification.absorption.push(i);
                        rules[i][rules[i].length - 1].verification.absorption.push(j);
                    }else{
                        rules[i][rules[i].length - 1].verification.absorption.push(j);
                    }
                }else{
                    //por�wnanie obu regu� 2
                    samePremises = absorptionComparison(rule2, rule1);

                    //sprawdzenie, czy regu�y s� poch�aniaj�ce
                    if( samePremises == rule2.length - 2 ){
                        if( rules[j][rules[j].length - 1].verification.absorption == null ){
                            rules[j][rules[j].length - 1].verification.absorption = [];
                            rules[j][rules[j].length - 1].verification.absorption.push(j);
                            rules[j][rules[j].length - 1].verification.absorption.push(i);
                        }else{
                            rules[j][rules[j].length - 1].verification.absorption.push(i);
                        }
                    }
                }

            }

            function absorptionComparison( rule1, rule2){
                var samePremises = 0;

                //sprawdzenie d�ugo�ci obu regu�
                if( rule1.length < rule2.length ){
                    //sprawdzenie atrybut�w konkluzji
                    if( rule1[0].attribute.name == rule2[0].attribute.name ){
                        //sprawdzenie warto�ci konkluzji
                        if( ( rule1[0].attribute.type == 'continous' && rule1[0].continousValue == rule2[0].continousValue )||
                            ( rule1[0].attribute.type != 'continous' && rule1[0].value.name == rule2[0].value.name )
                        ){
                            //sprawdzenie operator�w konkluzji
                            if( rule1[0].operator == rule2[0].operator ){
                                //przegl�d wszystkich przes�anek z 1 regu�y
                                for( var k = 1; k < rule1.length - 1; k ++ ){
                                    if( samePremises != k - 1 ) break;
                                    //przegl�d wszystkich przes�anek z 2 regu�y
                                    for( var l = 1; l < rule2.length - 1; l++ ){
                                        //por�wnanie atrybut�w przes�anek
                                        if( rule1[k].attribute.name == rule2[l].attribute.name ){
                                            //por�wnanie warto�ci przes�anek
                                            if( ( rule1[k].attribute.type == 'continous' && rule1[k].continousValue == rule2[l].continousValue )||
                                                ( rule1[k].attribute.type != 'continous' && rule1[k].value.name == rule2[l].value.name )
                                            ){
                                                //por�wnanie operator�w przes�anek
                                                if( rule1[k].operator == rule2[l].operator ){
                                                    samePremises++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return samePremises;
            }

            function samenessVerification( rule1, rule2, rules, i, j ){

                var samePremises = 0;
                //por�wnanie obu regu�
                samePremises = samenessComparison(rule1, rule2);

                //sprawdzenie, czy regu�y s� identyczne
                if( samePremises == rule1.length - 2 ){
                    if( rules[i][rules[i].length - 1].verification.sameness == null ){
                        rules[i][rules[i].length - 1].verification.sameness = [];
                        rules[i][rules[i].length - 1].verification.sameness.push(i);
                        rules[i][rules[i].length - 1].verification.sameness.push(j);
                    }else{
                        rules[i][rules[i].length - 1].verification.sameness.push(j);
                    }
                }

            }

            function doubledAttributesVerification( rule, doubledAttributesRules, x ){
                var attribute1, attribute2, isDoubled = false;
                for( var i = 0; i < rule.length - 1; i++ ){
                    attribute1 = rule[i].attribute.name;
                    for( var j = 0; j < rule.length - 1; j++ ){
                        if( i != j ){
                            attribute2 = rule[j].attribute.name;
                            if( attribute1 == attribute2 ){
                                doubledAttributesRules.push(x);
                                isDoubled = true;
                                break;
                            }
                        }
                    }
                    if( isDoubled ) break;
                }
            }

           function createRulesString( rule ){
               var txt = "";

               for( var i = 0; i < rule.length; i++ ) {
                   if( typeof rule[i] != 'undefined'){
                       if (i != rule.length - 1) {
                           if (i != 0) {
                               if( rule[i].isConclusion != 'undefined') {
                                   if (rule[i - 1].isConclusion == rule[i].isConclusion) {
                                       txt += "AND "
                                   } else {
                                       txt += "IF "
                                   }
                               }
                           }
                           txt += rule[i].attribute.name + " " + rule[i].operator + " ";

                           if (rule[i].attribute.type == 'continous') {
                               txt += rule[i].continousValue + " ";
                           } else {
                               txt += rule[i].value.name + " ";
                           }
                       }
                   }else{
                       txt = 'Incorrect rule!';
                       break;
                   }
               }

               return txt;
           }
        }
    ]);