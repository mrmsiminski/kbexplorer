/**
 * Created by M. on 01.04.2016.
 * Last edit: 02.06.2016 22:45
 */

angular
    .module( 'kbExplorer' )
    .controller( 'homeCtrl', [
        '$scope',
        '$rootScope',
        '$http',
        '$location',
        '$sessionStorage',
        '$window',
        '$state',
        'BasesR',
        'UsersR',
        'SharesR',
        function( $scope, $rootScope, $http, $location, $sessionStorage, $window, $state, BasesR, UsersR, SharesR ) {

            contentHeight();
            var result = $location.search();
            if( result.success ) {
                $location.search('success', null);
            }

            var homePage = this;
            homePage.creation = 0;
            homePage.validate = true;

            homePage.currentBase = {};

            /**
             * creating bases table
             * */
            $scope.bases = $rootScope.basesTable;
            if( $rootScope.basesTable.length == 0 ){
                createBasesTable();
            }

            function createBasesTable(){
                BasesR.getAll().$promise.then(function success(response){
                    for( var i = 0; i < response.length; i++ ){
                        response[i].tabPosition = i;
                        $rootScope.basesTable[i] = response[i];
                        $rootScope.hashMapB[response[i].id] = $rootScope.basesTable[i];
                    }
                }, function error(response){
                    console.log(response);
                });
                $scope.bases = $rootScope.basesTable;
            }

            /**
             * home page function
             * */
            homePage.animate = function(event){
                $rootScope.animate( event );
            };

            homePage.openBase = function( id ){
                $sessionStorage.currentBaseId = id;
                $state.go('base');
            };

            homePage.isCreate = function(){
                return homePage.creation == 1;
            };

            /**
             * Creating new base
             * */
            homePage.createModal = function(){
                homePage.creation = 1;
                $scope.modalTitle = "Creating";
                $scope.baseName = "";
                $scope.baseDescription = "";
                $("#baseModal").modal('show');
            };

            homePage.createBase = function(){

                var createData = {
                    "description": $scope.baseDescription,
                    "id": null,
                    "name": $scope.baseName,
                    "owner": null
                };
                BasesR.create( createData ).$promise.then(function success(response){

                    createData.id = response.id;
                    $rootScope.basesTable.push( createData )
                    $("#baseModal").modal('hide');

                }, function error(response){
                    console.log(response);
                    $("#baseModal").modal('hide');
                });
            };

            /**
             * editing base
             * */
            homePage.editModal = function(event, index){
                $rootScope.animateOff( event );
                homePage.creation = 0;
                homePage.currentBase = $rootScope.basesTable[index];
                $scope.modalTitle = "Editing";
                $scope.editPositionId = homePage.currentBase.id;
                $scope.editPositionIndex = index;
                $scope.baseName = homePage.currentBase.name;
                $scope.baseDescription = homePage.currentBase.description;

                homePage.validateUserPermission( homePage.currentBase.id, index, function(){
                    if( $rootScope.userHasPermission == true ){
                        $("#baseModal").modal('show');
                    }else if( $rootScope.userHasPermission == false ){
                        $rootScope.tempMsg = 'You have not permission to do that';
                        $("#infoModal").modal('show');

                    }
                } );
            };

            homePage.editBase = function(id, index){

                    var updateData = {
                        "description": $scope.baseDescription,
                        "id": id,
                        "name": $scope.baseName,
                        "owner": null
                    };

                    BasesR.update( angular.toJson( updateData ) ).$promise.then(function success(response){
                        $rootScope.basesTable[index].description = updateData.description;
                        $rootScope.basesTable[index].name = updateData.name;
                        $("#baseModal").modal('hide');
                    }, function error(response){
                        console.log(response);
                        $("#baseModal").modal('hide');
                    });
            };

            /**
             * deleting base
             * */
            homePage.deleteInfo = function( event, name, id, index ){
                $rootScope.animateOff( event );
                $scope.deletePositionName = name;
                $scope.deletePositionId = id;
                $scope.deletePositionIndex = index;
                homePage.validateUserPermission( id, index, function(){
                    if( $rootScope.userHasPermission == true ){
                        $('#deleteModal').modal('show');
                    }else if( $rootScope.userHasPermission == false ){
                        $rootScope.tempMsg = 'You have not permission to do that';
                        $("#infoModal").modal('show');
                    }
                } );
            };

            homePage.deleteBase = function( index, id ){
                BasesR.delete( {baseId: id}).$promise.then(function success(response){
                    $rootScope.basesTable.splice( index, 1 );
                    delete $rootScope.hashMapB[id];
                }, function error(response){
                    console.log(response);
                });
            };

            /**
             * jQuery
             * */
            $("#baseForm1").submit(function( event ) {
                alert( "Handler for .submit() called." );
                event.preventDefault();
            });

            /**
             * User permissions validation
             * */

            /*function validateCallback(){
                if( $rootScope.userHasPermission == true ){
                    $("#baseModal").modal('show');
                }else if( $rootScope.userHasPermission == false ){
                    console.log('You have not permission');

                }
            }*/

            homePage.validateUserPermission = function( id, index, validateCallback ){
                var user = $sessionStorage.loggedUser;
                var base = $rootScope.basesTable[index];

                //sprawdzenie, czy Admin
                for( var i = 0; i < user.authorities.length; i++ ){
                    if( user.authorities[i] == "ROLE_ADMIN" ){
                        $rootScope.userHasPermission = true;
                        //$rootScope.userSharePermission = true;
                        validateCallback();
                        return $rootScope.userHasPermission;
                    }else{
                        $rootScope.userHasPermission = false;
                        //$rootScope.userSharePermission = false;
                    }
                }



                //sprawdzenie, czy jest Ownerem
                if( user.login == base.owner.login ){
                    $rootScope.userHasPermission = true;
                    //$rootScope.userSharePermission = true;
                    validateCallback();
                    return $rootScope.userHasPermission;
                }else{
                    $rootScope.userHasPermission = false;
                    $rootScope.userSharePermission = false;
                }

                //sprawdzenie, czy posiada permission 'full'
                SharesR.getAll({baseId: id}).$promise.then(function success(response){
                    for( var i = 0; i < response.length; i ++ ){
                        if( response[i].user.login == user.login &&
                            response[i].permision == 'full' ){
                            $rootScope.userHasPermission = true;
                            validateCallback();
                            return $rootScope.userHasPermission;
                        }else{
                            $rootScope.userHasPermission = false;
                        }

                        /*if( response[i].user.login == user.login &&
                            response[i].permision == 'full' && user.authorities[i] == "ROLE_ADMIN" ||
                            response[i].user.login == user.login &&
                            response[i].permision == 'full' && user.login == base.owner.login ){
                            $rootScope.userSharePermission = true;
                            validateCallback();
                            return $rootScope.userSharePermission;
                        }else{
                            $rootScope.userSharePermission = false;
                        }*/
                    }
                    validateCallback();
                    return $rootScope.userHasPermission;
                }, function error(response){
                    console.log(response);
                });

            };

            /**
             * TESTING
             * */
            homePage.test = function(){
                var temp = {};
                temp.status = 404;
                var token = $sessionStorage.tokenData.access_token;
                console.log(token);
              if( $rootScope.errorHandler( temp ) ){
                  //alert("youpi!");
                  token = $sessionStorage.tokenData.access_token;
                  console.log( token );
                  setTimeout(function(){getBases()},2000);
                  //getBases();
              }else{
                  alert("nope");
              }
            };


        }
    ]);
